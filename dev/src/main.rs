use clap::{value_parser, App, Arg, ValueEnum};
use notify::{self, DebouncedEvent, RecursiveMode, Watcher};
use std::env;
use std::path::Path;
use std::process::{Child, Command};
use std::sync::mpsc::channel;
use std::sync::mpsc::Sender;
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;
use std::time::Duration;

#[derive(Clone, Copy)]
enum Message {
    RestartServer,
    RebuildWeb,
    RestartAll,
    Exit,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum WebMode {
    Online,
    Local,
    Disabled,
}

fn main() {
    let args = App::new("Affinity Dev Tools")
        .version("0.1")
        .arg(
            Arg::with_name("disable-server")
                .long("disable-server")
                .help("Don't run the server"),
        )
        .arg(
            Arg::with_name("web-mode")
                .long("web-mode")
                .help("What mode to run the web client in")
                .value_parser(value_parser!(WebMode))
                .default_value("online"),
        )
        .get_matches();

    let enable_server = !args.is_present("disable-server");
    let web_mode = *args
        .get_one::<WebMode>("web-mode")
        .expect("web-mode is missing");

    let commands = Arc::new(Mutex::new(CommandManager::new(enable_server, web_mode)));
    let (tx, rx) = channel();

    {
        let commands = Arc::clone(&commands);
        let tx = tx.clone();

        ctrlc::set_handler(move || {
            commands.lock().unwrap().stop();
            tx.send(Message::Exit).unwrap();
        })
        .expect("Error setting Ctrl-C handler");
    }

    create_watcher("game", &tx, Message::RestartAll, &[]);

    if enable_server {
        create_watcher("server", &tx, Message::RestartServer, &[]);
    }

    if web_mode != WebMode::Disabled {
        create_watcher(
            "web",
            &tx,
            Message::RebuildWeb,
            &["wasm", "generated", "dist", "www"],
        );
    }

    loop {
        let msg = rx.recv().unwrap();
        match msg {
            Message::RestartServer => commands.lock().unwrap().restart_server(),
            Message::RebuildWeb => CommandManager::build_wasm(),
            Message::RestartAll => commands.lock().unwrap().restart(),
            Message::Exit => break,
        }
    }
}

fn path_is_included<P, S>(path: P, ignored_paths: &[S]) -> bool
where
    P: AsRef<Path>,
    S: AsRef<str>,
{
    let path = path.as_ref();
    !ignored_paths
        .iter()
        .map(AsRef::as_ref)
        .any(|p| path.starts_with(p))
}

fn create_watcher(
    watch_path: &'static str,
    sender: &Sender<Message>,
    onchange_msg: Message,
    ignores: &[&str],
) -> JoinHandle<()> {
    let tx = sender.clone();
    let current_dir = env::current_dir().unwrap();
    let full_watch_path = current_dir.join(watch_path);
    let ignores: Vec<String> = ignores.iter().map(|p| p.to_string()).collect();

    std::thread::spawn(move || {
        let (watcher_tx, watcher_rx) = channel();
        let mut watcher = notify::watcher(watcher_tx, Duration::from_millis(500)).unwrap();
        watcher
            .watch(&full_watch_path, RecursiveMode::Recursive)
            .unwrap();

        loop {
            match watcher_rx.recv() {
                Ok(DebouncedEvent::Create(path))
                | Ok(DebouncedEvent::Write(path))
                | Ok(DebouncedEvent::Remove(path)) => {
                    if path_is_included(path.strip_prefix(&full_watch_path).unwrap(), &ignores) {
                        tx.send(onchange_msg).unwrap();
                    }
                }
                Ok(_event) => {}
                Err(e) => println!("Watch error: {:?}", e),
            }
        }
    })
}

struct CommandManager {
    server: Option<Child>,
    web: Option<Child>,
}

impl CommandManager {
    fn new(enable_server: bool, web_mode: WebMode) -> CommandManager {
        let server = if enable_server {
            Some(CommandManager::run_server())
        } else {
            None
        };

        let web = if web_mode != WebMode::Disabled {
            CommandManager::build_wasm();
            Some(CommandManager::run_web(web_mode == WebMode::Online))
        } else {
            None
        };

        CommandManager { server, web }
    }

    fn run_server() -> Child {
        Command::new("cargo")
            .args(&["run", "--package", "affinity_server"])
            .env("RUST_BACKTRACE", "1")
            .spawn()
            .unwrap()
    }

    fn build_wasm() {
        let status = Command::new("cargo")
            .args(&[
                "build",
                "--package",
                "affinity_web",
                "--target",
                "wasm32-unknown-unknown",
            ])
            .status()
            .unwrap();
        if !status.success() {
            return;
        }

        Command::new("wasm-bindgen")
            .args(&[
                "target/wasm32-unknown-unknown/debug/affinity_web.wasm",
                "--out-dir",
                "web/wasm",
            ])
            .status()
            .unwrap();
    }

    fn run_web(online: bool) -> Child {
        if cfg!(target_os = "windows") {
            Command::new("cmd")
                .current_dir("web")
                .env("AFFINITY_IS_ONLINE", online.to_string())
                .args(&["/C", "yarn", "start"])
                .spawn()
                .unwrap()
        } else {
            Command::new("yarn")
                .current_dir("web")
                .env("AFFINITY_IS_ONLINE", online.to_string())
                .args(&["start"])
                .spawn()
                .unwrap()
        }
    }

    fn stop(&mut self) {
        if let Some(server) = &mut self.server {
            server.kill().unwrap_or(());
        }

        if let Some(web) = &mut self.web {
            web.kill().unwrap_or(());
        }
    }

    fn restart_server(&mut self) {
        let server = self
            .server
            .as_mut()
            .expect("Cannot restart server when server is not enabled");
        server.kill().unwrap_or(());

        self.server = Some(CommandManager::run_server());
    }

    fn restart(&mut self) {
        if self.web.is_some() {
            CommandManager::build_wasm();
        }

        if let Some(server) = &mut self.server {
            server.kill().unwrap_or(());
            self.server = Some(CommandManager::run_server());
        }
    }
}
