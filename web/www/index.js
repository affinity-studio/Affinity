import { Game } from "../wasm/affinity_web.js";

const lpad = (s, width, char) => {
  return s.length >= width
    ? s
    : (new Array(width).join(char) + s).slice(-width);
};

const assetsUrl = "/assets/";

// Game.test();
// IS_ONLINE is defined build time
const online = IS_ONLINE;
const game = Game.new(online);

if (!online) {
  window
    .fetch(`${assetsUrl}/test_level.json`)
    .then((response) => {
      if (response.ok) {
        return response.arrayBuffer();
      } else {
        return Promise.reject(`Got ${response.status}`);
      }
    })
    .then((buffer) => new Uint8Array(buffer))
    .then((array) => game.on_level_file_loaded(array))
    .catch((error) => {
      console.error(`Failed to load level file: ${error}`);
    });
}

const fpsDisplay = document.body.querySelector("#fps-display");
const fpsUpdateTimeMs = 500;
let timeMsSinceLastFps = 0;
let framesSinceLastFps = 0;

const updateFpsDisplay = (deltaTimeMs) => {
  framesSinceLastFps += 1;
  timeMsSinceLastFps += deltaTimeMs;

  if (timeMsSinceLastFps >= fpsUpdateTimeMs) {
    const fps = framesSinceLastFps / (timeMsSinceLastFps / 1000);
    fpsDisplay.textContent = `FPS: ${lpad(Math.round(fps), 3, "0")}`;
    timeMsSinceLastFps = 0;
    framesSinceLastFps = 0;
  }
};

const frame = (now, prev) => {
  const deltaTimeMs = now - prev;
  updateFpsDisplay(deltaTimeMs);

  game.step(deltaTimeMs);

  let queuedAsset = game.pop_asset_queue();
  while (queuedAsset !== undefined) {
    let name = queuedAsset;
    console.log("Loading asset", name);
    window
      .fetch(`${assetsUrl}${name}`)
      .then((response) => {
        if (response.ok) {
          return response.arrayBuffer();
        } else {
          return Promise.reject(`Got ${response.status}`);
        }
      })
      .then((buffer) => new Uint8Array(buffer))
      .then((array) => game.on_asset_fetched(name, array))
      .catch((error) => {
        console.error(`Failed to load '${name}': ${error}`);
      });
    queuedAsset = game.pop_asset_queue();
  }

  requestAnimationFrame((next) => frame(next, now));
};

requestAnimationFrame((now) => frame(now, now));
