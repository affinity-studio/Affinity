use std::fs::{File, create_dir};
use std::io::{self, prelude::*, Write};
use std::path::Path;

fn main() -> Result<(), io::Error> {
    let out_dir = Path::new("./generated");
    if !out_dir.exists() {
        create_dir(&out_dir)?;
    }

    let dest_path = Path::new(&out_dir).join("shaders.rs");
    let mut out_file = File::create(&dest_path)?;

    let mut vertex_shader = String::new();
    File::open("./shaders/vertex.glsl")?.read_to_string(&mut vertex_shader)?;

    let mut fragment_shader = String::new();
    File::open("./shaders/fragment.glsl")?.read_to_string(&mut fragment_shader)?;

    out_file.write_all(
        format!(
            r##"
pub const VERTEX_SHADER: &str = r#"{vertex_shader}"#;
pub const FRAGMENT_SHADER: &str = r#"{fragment_shader}"#;
"##,
            vertex_shader = vertex_shader,
            fragment_shader = fragment_shader
        )
        .as_bytes(),
    )?;

    Ok(())
}
