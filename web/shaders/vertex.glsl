#version 300 es

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

// Instanced
in vec2 position;

// Per instance
in vec2 textureOffset;
in vec2 textureSize;
in vec2 translation;
in vec2 scale;
in float rotation;

out vec2 textureCoordinate;

void main() {
  vec2 transformedPosition = position * scale;
  transformedPosition = vec2(
    transformedPosition.x * cos(rotation) - transformedPosition.y * sin(rotation),
    transformedPosition.y * cos(rotation) + transformedPosition.x * sin(rotation)
  );
  transformedPosition = transformedPosition + translation;

  gl_Position = projectionMatrix * viewMatrix * vec4(transformedPosition, 0.0, 1.0);
  textureCoordinate = (position + vec2(0.5, 0.5)) * textureSize + textureOffset;
}