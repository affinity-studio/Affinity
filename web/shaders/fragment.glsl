#version 300 es

precision highp float;

uniform sampler2D textureSampler;

in vec2 textureCoordinate;

out vec4 outColor;

void main() {
  outColor = texture(textureSampler, textureCoordinate);
}