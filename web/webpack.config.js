const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { DefinePlugin } = require('webpack');

module.exports = {
    entry: "./www/bootstrap.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bootstrap.js",
    },
    mode: "development",
    plugins: [
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: '../assets',
                    to: 'assets',
                    globOptions: {
                        ignore: [
                            '.git/**/*',
                            '.git*',
                        ]
                    }
                },
                {
                    from: './www/index.html',
                    to: '.',
                },
                {
                    from: './www/main.css',
                    to: '.',
                },
            ]
        }),
        new DefinePlugin({
            IS_ONLINE: process.env.AFFINITY_IS_ONLINE,
        })
    ],
}