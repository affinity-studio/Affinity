use crate::webgl::ResizeEvent;
use affinity_game::render::components::texture::Section;
use affinity_game::{
    assets::{Asset, Assets, Handle},
    camera::ActiveCamera,
    components::{Orientation, Position, Scale},
    core::ecs::SystemName,
    geom::Rect,
    render::components::{LoadedTexture, LoadingTexture, Texture},
};
use hashbrown::HashMap;
use image::{self, DynamicImage};
use log::{debug, error, info};
use nalgebra::{Matrix4, Rotation3, Unit, Vector2, Vector3};
use nalgebra_glm as glm;
use serde_derive::Deserialize;
use shrev::{EventChannel, ReaderId};
use specs::prelude::*;
use specs::world::EntitiesRes;
use std::convert::TryFrom;
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{
    self as web, HtmlCanvasElement, WebGl2RenderingContext, WebGlProgram, WebGlShader,
    WebGlTexture, WebGlUniformLocation,
};

include!("../../generated/shaders.rs");

const PIXELS_PER_POINT: f32 = 32.0;
const POINTS_PER_PIXEL: f32 = 1.0 / PIXELS_PER_POINT;

pub struct Renderer {
    positions: [f32; 10],
    instance_data: HashMap<usize, (usize, Vec<f32>)>,
    num_instances: usize,
    bound_texture: Option<usize>,
    projection_matrix: Matrix4<f32>,
    projection_matrix_location: Option<WebGlUniformLocation>,
    view_matrix_location: Option<WebGlUniformLocation>,
    textures: Vec<TextureInfo>,
    webgl_context: Option<WebGl2RenderingContext>,
    asset_index: AssetIndex,
    source_texture_handles: HashMap<String, Handle>,
    handle_texture_map: HashMap<Handle, usize>,
    resize_event_reader: Option<ReaderId<ResizeEvent>>,
}

struct TextureInfo {
    texture: WebGlTexture,
    width: u32,
    height: u32,
}

impl Default for Renderer {
    fn default() -> Renderer {
        Renderer {
            positions: [-0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5, -0.5, -0.5, 0.5],
            instance_data: Default::default(),
            num_instances: 0,
            bound_texture: None,
            projection_matrix: Matrix4::identity(),
            projection_matrix_location: None,
            view_matrix_location: None,
            textures: vec![],
            webgl_context: None,
            asset_index: Default::default(),
            source_texture_handles: Default::default(),
            handle_texture_map: Default::default(),
            resize_event_reader: None,
        }
    }
}

impl<'a> System<'a> for Renderer {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Orientation>,
        ReadStorage<'a, Scale>,
        ReadStorage<'a, Texture>,
        WriteStorage<'a, LoadingTexture>,
        WriteStorage<'a, LoadedTexture>,
        Write<'a, Assets>,
        Read<'a, EventChannel<ResizeEvent>>,
        ReadExpect<'a, ActiveCamera>,
    );

    fn run(
        &mut self,
        (
            entities,
            positions,
            orientations,
            scales,
            textures,
            mut loading_textures,
            mut loaded_textures,
            mut assets,
            resize_events_chan,
            active_camera,
        ): Self::SystemData,
    ) {
        let resize_events = resize_events_chan.read(self.resize_event_reader.as_mut().unwrap());
        if let Some(event) = resize_events.last() {
            self.resize(event.width, event.height).unwrap();
        }

        if let Some(handle) = self.asset_index.loading() {
            self.load_asset_index(handle, &mut assets);
        }

        if let AssetIndex::Loaded(_) = self.asset_index {
            self.start_texture_loads(&entities, &textures, &mut loading_textures, &mut assets);
            self.finish_texture_loads(
                &entities,
                loading_textures,
                &mut loaded_textures,
                &mut assets,
            );
        }

        self.render(
            &positions,
            &orientations,
            &scales,
            &loaded_textures,
            &active_camera,
        );
    }

    fn setup(&mut self, res: &mut World) {
        Self::SystemData::setup(res);

        {
            let mut resize_event_channel = res.fetch_mut::<EventChannel<ResizeEvent>>();
            self.resize_event_reader = Some(resize_event_channel.register_reader());
        }

        match self.init_webgl() {
            Ok(_) => {}
            Err(err) => {
                error!("Failed to initialize WebGL: {:?}", err);
                panic!();
            }
        };

        let assets = match res.get_mut::<Assets>() {
            Some(assets) => assets,
            None => {
                error!("Assets resource not available");
                panic!();
            }
        };

        let assets_index_handle = assets.load("index.json");

        if let Some(asset) = assets.get(&assets_index_handle) {
            let index = match serde_json::from_slice(&asset.data) {
                Ok(index) => index,
                Err(err) => {
                    error!("Failed to deserialize asset index: {:?}", err);
                    panic!();
                }
            };
            self.asset_index = AssetIndex::Loaded(index);
        } else {
            self.asset_index = AssetIndex::Loading(assets_index_handle);
        }
    }
}

impl SystemName for Renderer {
    const NAME: &'static str = "webgl_renderer";
}

impl Renderer {
    pub fn init_webgl(&mut self) -> Result<(), JsValue> {
        debug!("Creating canvas");

        let window = web::window().ok_or("Could not access `window`")?;

        let document = window
            .document()
            .ok_or("Could not access `window.document`")?;
        let body = document
            .body()
            .ok_or("Could not access `window.document.body`")?;
        let canvas = document
            .create_element("canvas")?
            .dyn_into::<HtmlCanvasElement>()
            .map_err(|_| "'canvas' element is not a HtmlCanvasElement")?;

        (body.as_ref() as &web::Node).append_child(canvas.as_ref() as &web::Node)?;

        debug!("Creating WebGL2 context");

        let context = canvas
            .get_context("webgl2")?
            .ok_or("Could not get webgl2 context")?
            .dyn_into::<WebGl2RenderingContext>()
            .map_err(|_| "Canvas context is not a WebGl2RenderingContext")?;

        context.clear_color(0.025, 0.037, 0.01, 1.0);
        context.disable(WebGl2RenderingContext::DEPTH_TEST);
        context.enable(WebGl2RenderingContext::BLEND);
        context.blend_func(
            WebGl2RenderingContext::SRC_ALPHA,
            WebGl2RenderingContext::ONE_MINUS_SRC_ALPHA,
        );

        debug!("Compiling shaders");

        let vertex_shader = compile_shader(
            &context,
            WebGl2RenderingContext::VERTEX_SHADER,
            VERTEX_SHADER,
        )?;
        let fragment_shader = compile_shader(
            &context,
            WebGl2RenderingContext::FRAGMENT_SHADER,
            FRAGMENT_SHADER,
        )?;

        debug!("Linking shaders");

        let shader_program = link_program(&context, [vertex_shader, fragment_shader].iter())?;

        context.use_program(Some(&shader_program));

        debug!("Getting uniform locations");

        let texture_sampler_location = context
            .get_uniform_location(&shader_program, "textureSampler")
            .ok_or("Could not get location of textureSampler uniform")?;

        context.uniform1i(Some(&texture_sampler_location), 0);

        let projection_matrix_location = context
            .get_uniform_location(&shader_program, "projectionMatrix")
            .ok_or("Could not get location of projectionMatrix uniform")?;

        let projection_matrix_data = self.projection_matrix.as_slice();
        context.uniform_matrix4fv_with_f32_array(
            Some(&projection_matrix_location),
            false,
            projection_matrix_data,
        );

        self.projection_matrix_location = Some(projection_matrix_location);

        let view_matrix_location = context
            .get_uniform_location(&shader_program, "viewMatrix")
            .ok_or("Could not get location of viewMatrix uniform")?;

        self.view_matrix_location = Some(view_matrix_location);

        debug!("Getting attrib locations");

        let position_attr_location =
            u32::try_from(context.get_attrib_location(&shader_program, "position"))
                .map_err(|_| "Failed to get position attr location")?;
        let rotation_attr_location =
            u32::try_from(context.get_attrib_location(&shader_program, "rotation"))
                .map_err(|_| "Failed to get rotation attr location")?;
        let translation_attr_location =
            u32::try_from(context.get_attrib_location(&shader_program, "translation"))
                .map_err(|_| "Failed to get translation attr location")?;
        let scale_attr_location =
            u32::try_from(context.get_attrib_location(&shader_program, "scale"))
                .map_err(|_| "Failed to get scale attr location")?;
        let texture_offset_attr_location =
            u32::try_from(context.get_attrib_location(&shader_program, "textureOffset"))
                .map_err(|_| "Failed to get textureOffset attr location")?;
        let texture_size_attr_location =
            u32::try_from(context.get_attrib_location(&shader_program, "textureSize"))
                .map_err(|_| "Failed to get textureSize attr location")?;

        debug!("Creating vao");

        let vao = context
            .create_vertex_array()
            .ok_or("Could not create VAO")?;
        context.bind_vertex_array(Some(&vao));

        debug!("Creating position VBO");

        let position_buffer = context
            .create_buffer()
            .ok_or("Failed creating position vertex array buffer")?;
        context.bind_buffer(WebGl2RenderingContext::ARRAY_BUFFER, Some(&position_buffer));

        {
            debug!("Uploading position data");

            let position_data_array =
                js_sys::Float32Array::new(&JsValue::from(self.positions.len() as u32));
            for (i, f) in self.positions.iter().enumerate() {
                position_data_array.fill(*f, i as u32, (i + 1) as u32);
            }
            context.buffer_data_with_opt_array_buffer(
                WebGl2RenderingContext::ARRAY_BUFFER,
                Some(&position_data_array.buffer()),
                WebGl2RenderingContext::STATIC_DRAW,
            );
        }

        context.vertex_attrib_pointer_with_i32(
            position_attr_location,
            2,
            WebGl2RenderingContext::FLOAT,
            false,
            0,
            0,
        );
        context.enable_vertex_attrib_array(position_attr_location);

        debug!("Creating instance VBO");

        let instance_data_buffer = context.create_buffer();
        context.bind_buffer(
            WebGl2RenderingContext::ARRAY_BUFFER,
            instance_data_buffer.as_ref(),
        );

        const FLOAT_SIZE: i32 = 4;
        const NUM_COMPONENTS: i32 = 2 + 2 + 2 + 2 + 1;
        const STRIDE: i32 = FLOAT_SIZE * NUM_COMPONENTS;

        context.vertex_attrib_pointer_with_i32(
            translation_attr_location,
            2,
            WebGl2RenderingContext::FLOAT,
            false,
            STRIDE,
            0,
        );
        context.vertex_attrib_divisor(translation_attr_location, 1);
        context.enable_vertex_attrib_array(translation_attr_location);

        context.vertex_attrib_pointer_with_i32(
            scale_attr_location,
            2,
            WebGl2RenderingContext::FLOAT,
            false,
            STRIDE,
            2 * FLOAT_SIZE,
        );
        context.vertex_attrib_divisor(scale_attr_location, 1);
        context.enable_vertex_attrib_array(scale_attr_location);

        context.vertex_attrib_pointer_with_i32(
            texture_offset_attr_location,
            2,
            WebGl2RenderingContext::FLOAT,
            false,
            STRIDE,
            4 * FLOAT_SIZE,
        );
        context.vertex_attrib_divisor(texture_offset_attr_location, 1);
        context.enable_vertex_attrib_array(texture_offset_attr_location);

        context.vertex_attrib_pointer_with_i32(
            texture_size_attr_location,
            2,
            WebGl2RenderingContext::FLOAT,
            false,
            STRIDE,
            6 * FLOAT_SIZE,
        );
        context.vertex_attrib_divisor(texture_size_attr_location, 1);
        context.enable_vertex_attrib_array(texture_size_attr_location);

        context.vertex_attrib_pointer_with_i32(
            rotation_attr_location,
            1,
            WebGl2RenderingContext::FLOAT,
            false,
            STRIDE,
            8 * FLOAT_SIZE,
        );
        context.vertex_attrib_divisor(rotation_attr_location, 1);
        context.enable_vertex_attrib_array(rotation_attr_location);

        self.webgl_context = Some(context);

        self.resize(body.client_width() as u32, body.client_height() as u32)?;

        Ok(())
    }

    pub fn resize(&mut self, width: u32, height: u32) -> Result<(), JsValue> {
        debug!("resizing to {}x{}", width, height);

        let context = self.webgl_context.as_ref().ok_or("webgl_context not set")?;
        let canvas: HtmlCanvasElement = context
            .canvas()
            .ok_or("Could not get canvas from context")?
            .dyn_into()?;

        canvas.set_width(width);
        canvas.set_height(height);

        context.viewport(0, 0, width as i32, height as i32);

        let world_width = width as f32 / PIXELS_PER_POINT;
        let world_height = height as f32 / PIXELS_PER_POINT;

        self.projection_matrix = glm::ortho(
            -world_width / 2.0,
            world_width / 2.0,
            -world_height / 2.0,
            world_height / 2.0,
            -1.0,
            1.0,
        );

        let projection_matrix_location = self
            .projection_matrix_location
            .as_ref()
            .ok_or("projection_matrix_location not set")?;
        let projection_matrix_data = self.projection_matrix.as_slice();
        context.uniform_matrix4fv_with_f32_array(
            Some(projection_matrix_location),
            false,
            projection_matrix_data,
        );

        Ok(())
    }

    fn create_texture(&mut self, asset: &Asset) -> Result<usize, JsValue> {
        info!("Creating texture for {}", asset.name);

        let context = self.webgl_context.as_ref().unwrap();

        let texture = context.create_texture().ok_or("Could not create texture")?;
        context.bind_texture(WebGl2RenderingContext::TEXTURE_2D, Some(&texture));

        let image = image::load_from_memory(&asset.data)
            .map_err(|err| format!("Failed loading image: {}", err))?;
        let image = image.flipv();
        let mut image = if let DynamicImage::ImageRgba8(image) = image {
            image
        } else {
            return Err("Texture image must be RGBA8".into());
        };

        let level = 0;
        let border = 0;
        let pixels = image
            .pixels_mut()
            .flat_map(|pixel| pixel.0.iter().cloned())
            .collect::<Vec<_>>();

        context.tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array(
            WebGl2RenderingContext::TEXTURE_2D,
            level,
            WebGl2RenderingContext::RGBA as i32,
            image.width() as i32,
            image.height() as i32,
            border,
            WebGl2RenderingContext::RGBA,
            WebGl2RenderingContext::UNSIGNED_BYTE,
            Some(&pixels),
        )?;

        context.tex_parameteri(
            WebGl2RenderingContext::TEXTURE_2D,
            WebGl2RenderingContext::TEXTURE_WRAP_S,
            WebGl2RenderingContext::CLAMP_TO_EDGE as i32,
        );
        context.tex_parameteri(
            WebGl2RenderingContext::TEXTURE_2D,
            WebGl2RenderingContext::TEXTURE_WRAP_T,
            WebGl2RenderingContext::CLAMP_TO_EDGE as i32,
        );
        context.tex_parameteri(
            WebGl2RenderingContext::TEXTURE_2D,
            WebGl2RenderingContext::TEXTURE_MIN_FILTER,
            WebGl2RenderingContext::NEAREST as i32,
        );
        context.tex_parameteri(
            WebGl2RenderingContext::TEXTURE_2D,
            WebGl2RenderingContext::TEXTURE_MAG_FILTER,
            WebGl2RenderingContext::NEAREST as i32,
        );

        let index = self.textures.len();
        self.textures.push(TextureInfo {
            texture,
            width: image.width(),
            height: image.height(),
        });

        debug!("Texture created and assigned index {}", index);

        Ok(index)
    }

    fn load_asset_index(&mut self, handle: Handle, assets: &mut Assets) {
        if let Some(asset) = assets.get(&handle) {
            info!("Asset index loaded");

            let index: Vec<AssetIndexEntry> = match serde_json::from_slice(&asset.data) {
                Ok(index) => index,
                Err(err) => {
                    error!("Failed to deserialize asset index: {:?}", err);
                    panic!();
                }
            };

            debug!("Asset index: {:?}", index);

            let mut source_textures: Vec<_> = index.iter().map(|e| &e.src).collect();
            source_textures.sort_unstable();
            source_textures.dedup();

            info!("Loading source textures: {:?}", &source_textures);

            let source_handles: Vec<_> =
                source_textures.iter().map(|src| assets.load(src)).collect();

            for handle in source_handles {
                self.source_texture_handles
                    .insert(handle.name().into(), handle);
            }

            self.asset_index = AssetIndex::Loaded(index);
        }
    }

    fn start_texture_loads(
        &self,
        entities: &EntitiesRes,
        textures: &ReadStorage<Texture>,
        loading_textures: &mut WriteStorage<LoadingTexture>,
        assets: &mut Assets,
    ) {
        // TODO: Consider using modification events to improve performance.
        for (entity, texture) in (entities, textures).join() {
            if !loading_textures.contains(entity) {
                let loading_texture = match texture {
                    Texture::Cutout { source, section } => {
                        let source_handle = self.source_texture_handles.get(source).unwrap();
                        LoadingTexture {
                            handle: source_handle.clone(),
                            section: Some(section.clone()),
                        }
                    }
                    Texture::Named(name) => {
                        if let Some(index_entry) = self.asset_index.find(name) {
                            let source_handle =
                                self.source_texture_handles.get(&index_entry.src).unwrap();
                            LoadingTexture {
                                handle: source_handle.clone(),
                                section: Some(Section {
                                    area: index_entry.sprite,
                                    offset: Vector2::new(
                                        (index_entry.origin.x - index_entry.sprite.x) as i32,
                                        -((index_entry.origin.y - index_entry.sprite.y) as i32),
                                    ),
                                }),
                            }
                        } else {
                            let handle = assets.load(name);

                            LoadingTexture {
                                handle,
                                section: None,
                            }
                        }
                    }
                };
                loading_textures.insert(entity, loading_texture).unwrap();
            }
        }
    }

    fn finish_texture_loads(
        &mut self,
        entities: &EntitiesRes,
        mut loading_textures: WriteStorage<LoadingTexture>,
        loaded_textures: &mut WriteStorage<LoadedTexture>,
        assets: &mut Assets,
    ) {
        let mut new_loaded_textures = vec![];

        for (entity, loading_texture) in (entities, &mut loading_textures).join() {
            let handle = &loading_texture.handle;

            let texture_index = if let Some(texture_index) = self.handle_texture_map.get(handle) {
                Some(texture_index)
            } else if let Some(asset) = assets.get(handle) {
                debug!("Asset {} loaded: Creating WebGL texture", handle.name());

                let info = match self.create_texture(asset) {
                    Ok(info) => info,
                    Err(err) => {
                        error!("Failed to create texture: {:?}", err);
                        panic!();
                    }
                };

                self.handle_texture_map.insert(handle.clone(), info);
                self.handle_texture_map.get(handle)
            } else {
                None
            };

            if let Some(index) = texture_index {
                let texture = if let Some(section) = &loading_texture.section {
                    // debug!("Texture {} is a section of an atlas", name);

                    LoadedTexture {
                        index: *index,
                        size: Vector2::new(section.area.w, section.area.h),
                        offset: Vector2::new(section.area.x, section.area.y),
                        origin: section.offset,
                    }
                } else {
                    // debug!("Texture {} is independent", name);

                    let info = &self.textures[*index];
                    LoadedTexture {
                        index: *index,
                        size: Vector2::new(info.width, info.height),
                        ..LoadedTexture::new()
                    }
                };

                // debug!(
                //     "Texture {} loaded. Replacing LoadingTexture with Texture",
                //     name
                // );
                new_loaded_textures.push((entity, texture));
            }
        }

        for (entity, texture) in new_loaded_textures {
            loading_textures.remove(entity);
            loaded_textures.insert(entity, texture).unwrap();
        }
    }

    fn render(
        &mut self,
        positions: &ReadStorage<Position>,
        orientations: &ReadStorage<Orientation>,
        scales: &ReadStorage<Scale>,
        loaded_textures: &WriteStorage<LoadedTexture>,
        active_camera: &ActiveCamera,
    ) {
        self.instance_data.clear();
        self.num_instances = 0;

        for (position, orientation, scale, texture) in
            (positions, orientations, scales, loaded_textures).join()
        {
            let (ref mut num_instances, ref mut instance_data) = self
                .instance_data
                .entry(texture.index)
                .or_insert((0, vec![]));

            let scale = Vector2::new(
                texture.size.x as f32 * scale.0.x * POINTS_PER_PIXEL * 1.0,
                texture.size.y as f32 * scale.0.y * POINTS_PER_PIXEL * 1.0,
            );

            let position = position.0
                + Vector2::new(
                    texture.origin.x as f32 * POINTS_PER_PIXEL,
                    texture.origin.y as f32 * POINTS_PER_PIXEL,
                );
            let texture_info = &self.textures[texture.index];
            let texture_size = Vector2::new(
                texture.size.x as f32 / texture_info.width as f32,
                texture.size.y as f32 / texture_info.height as f32,
            );
            let texture_offset = Vector2::new(
                texture.offset.x as f32 / texture_info.width as f32,
                (1.0 - texture_size.y) - (texture.offset.y as f32 / texture_info.height as f32),
            );
            // debug!("Index: {}", texture.index);
            // debug!("Size: {:?}", size);
            // debug!("Offset: {:?}", offset);

            instance_data.reserve(9);
            instance_data.extend_from_slice(position.as_slice());
            instance_data.extend_from_slice(scale.as_slice());
            instance_data.extend_from_slice(texture_offset.as_slice()); // texture offset
            instance_data.extend_from_slice(texture_size.as_slice()); // texture size
            instance_data.push(orientation.0);

            *num_instances += 1;
        }

        let context = self.webgl_context.as_ref().unwrap();

        let view_matrix = create_view_matrix(positions, orientations, scales, active_camera);

        let view_matrix_data = view_matrix.as_slice();
        context.uniform_matrix4fv_with_f32_array(
            Some(self.view_matrix_location.as_ref().unwrap()),
            false,
            view_matrix_data,
        );

        context.clear(WebGl2RenderingContext::COLOR_BUFFER_BIT);

        for (texture_index, (num_instances, instance_data)) in &self.instance_data {
            let instance_data_array =
                js_sys::Float32Array::new(&JsValue::from(instance_data.len() as u32));
            for (i, f) in instance_data.iter().enumerate() {
                instance_data_array.fill(*f, i as u32, (i + 1) as u32);
            }
            context.buffer_data_with_opt_array_buffer(
                WebGl2RenderingContext::ARRAY_BUFFER,
                Some(&instance_data_array.buffer()),
                WebGl2RenderingContext::STREAM_DRAW,
            );

            match self.bound_texture {
                Some(bound_texture) if bound_texture == *texture_index => {}
                _ => {
                    let texture_info = &self.textures[*texture_index];
                    context.active_texture(WebGl2RenderingContext::TEXTURE0);
                    context.bind_texture(
                        WebGl2RenderingContext::TEXTURE_2D,
                        Some(&texture_info.texture),
                    );

                    self.bound_texture = Some(*texture_index);
                }
            }

            let num_vertices = (self.positions.len() / 2) as i32;
            let num_instances = *num_instances as i32;
            context.draw_arrays_instanced(
                WebGl2RenderingContext::TRIANGLE_STRIP,
                0,
                num_vertices,
                num_instances,
            );
        }
    }
}

fn compile_shader(
    context: &WebGl2RenderingContext,
    shader_type: u32,
    source: &str,
) -> Result<WebGlShader, String> {
    let shader = context
        .create_shader(shader_type)
        .ok_or_else(|| String::from("Unable to create shader object"))?;
    context.shader_source(&shader, source);
    context.compile_shader(&shader);

    if context
        .get_shader_parameter(&shader, WebGl2RenderingContext::COMPILE_STATUS)
        .as_bool()
        .unwrap_or(false)
    {
        Ok(shader)
    } else {
        Err(context
            .get_shader_info_log(&shader)
            .unwrap_or_else(|| "Unknown error creating shader".into()))
    }
}

fn link_program<'a, T>(context: &WebGl2RenderingContext, shaders: T) -> Result<WebGlProgram, String>
where
    T: IntoIterator<Item = &'a WebGlShader>,
{
    let program = context
        .create_program()
        .ok_or_else(|| String::from("Unable to create shader object"))?;
    for shader in shaders {
        context.attach_shader(&program, shader)
    }
    context.link_program(&program);

    if context
        .get_program_parameter(&program, WebGl2RenderingContext::LINK_STATUS)
        .as_bool()
        .unwrap_or(false)
    {
        Ok(program)
    } else {
        Err(context
            .get_program_info_log(&program)
            .unwrap_or_else(|| "Unknown error creating program object".into()))
    }
}

#[derive(Deserialize, Debug)]
struct AssetIndexEntry {
    src: String,
    name: Option<String>, // TODO: Make this required
    sprite: Rect<u32>,
    origin: Rect<u32>,
}

enum AssetIndex {
    Loaded(Vec<AssetIndexEntry>),
    Loading(Handle),
    None,
}

impl Default for AssetIndex {
    fn default() -> AssetIndex {
        AssetIndex::None
    }
}

impl AssetIndex {
    fn find(&self, name: &str) -> Option<&AssetIndexEntry> {
        match self {
            AssetIndex::Loaded(asset_index) => {
                asset_index.iter().find(|e| e.name.as_deref() == Some(name))
            }
            _ => None,
        }
    }

    fn loading(&self) -> Option<Handle> {
        if let AssetIndex::Loading(handle) = self {
            Some(handle.clone())
        } else {
            None
        }
    }
}

fn create_view_matrix<'a>(
    positions: &ReadStorage<'a, Position>,
    orientations: &ReadStorage<'a, Orientation>,
    scales: &ReadStorage<'a, Scale>,
    active_camera: &ActiveCamera,
) -> Matrix4<f32> {
    let mut matrix = Matrix4::identity();

    if let Some(scale) = scales.get(active_camera.0) {
        matrix *= Matrix4::new_nonuniform_scaling(&Vector3::new(scale.0.x, scale.0.y, 1.0));
    }

    if let Some(position) = positions.get(active_camera.0) {
        matrix *= Matrix4::new_translation(&Vector3::new(-position.0.x, -position.0.y, 0.0));
    }

    if let Some(orientation) = orientations.get(active_camera.0) {
        let rotation_matrix = Rotation3::from_axis_angle(
            &Unit::new_unchecked(Vector3::new(0.0, 0.0, -1.0)),
            -orientation.0,
        )
        .to_homogeneous();
        matrix *= rotation_matrix;
    }

    matrix
}
