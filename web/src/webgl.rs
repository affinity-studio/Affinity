pub use self::{renderer::Renderer};

mod renderer;

pub struct ResizeEvent {
    pub width: u32,
    pub height: u32,
}