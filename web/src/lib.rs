use affinity_game::net::client::state_update::ClientStateUpdate;
use affinity_game::net::client::ReceivedServerStateUpdates;
use affinity_game::{
    self,
    assets::{Asset, Assets},
    input::{self, Key},
    net::Message,
    resources::{DeltaTime, Random},
    time::DeltaTimeAccumulator,
    GameMode,
};
use js_sys::{ArrayBuffer, Uint8Array};
use log::{self, debug, info};
use rand::prelude::*;
use rand_xorshift::XorShiftRng;
use shrev::EventChannel;
use specs::{Dispatcher, World, WorldExt};
use std::{
    cell::{Ref, RefCell},
    rc::Rc,
    time::Duration,
};
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{self as web, BinaryType, Event, KeyboardEvent, MessageEvent, WebSocket, Window};

use crate::webgl::{Renderer as WebGlRenderer, ResizeEvent};

mod webgl;

type JsResult<T> = Result<T, JsValue>;
type EventListener = Closure<dyn FnMut(Event)>;

#[wasm_bindgen]
pub struct Game {
    world: Rc<RefCell<World>>,
    dispatcher: Dispatcher<'static, 'static>,
    accumulator: DeltaTimeAccumulator,
    buffer: Vec<u8>,
    mode: GameMode,

    // This is kept in order to drop the listener Closures when the Game is dropped.
    #[allow(dead_code)]
    event_listeners: Vec<EventListener>,

    websocket: Option<Rc<WebSocket>>,
}

impl Game {
    fn step_fixed(&mut self, dt: Duration) {
        let mut world = self.world.borrow_mut();

        if self.accumulator.is_consumable() {
            {
                let mut deltatime = world.write_resource::<DeltaTime>();
                deltatime.actual = dt;
                deltatime.fixed = Some(self.accumulator.consumption_step());
            }

            while self.accumulator.consume() {
                self.dispatcher.dispatch(&world);
                world.maintain();
            }
        }
    }

    fn step_loose(&mut self, dt: Duration) {
        let mut world = self.world.borrow_mut();

        {
            let mut deltatime = world.write_resource::<DeltaTime>();
            deltatime.actual = dt;
            deltatime.fixed = None;
        }

        self.dispatcher.dispatch(&world);
        world.maintain();
    }

    fn setup_event_listeners(
        window: &Window,
        world: &Rc<RefCell<World>>,
    ) -> Result<Vec<EventListener>, JsValue> {
        Ok(vec![
            Game::setup_keydown_listener(window, world)?,
            Game::setup_keyup_listener(window, world)?,
            Game::setup_resize_listener(window, world)?,
        ])
    }

    fn setup_keydown_listener(
        window: &Window,
        world: &Rc<RefCell<World>>,
    ) -> JsResult<EventListener> {
        {
            let world = Rc::clone(world);

            let on_keydown = Closure::wrap(Box::new(move |event: Event| {
                let event: KeyboardEvent = event.unchecked_into();
                Game::on_key_down(world.borrow(), &event.code());
            }) as Box<dyn FnMut(Event)>);

            window
                .add_event_listener_with_callback("keydown", on_keydown.as_ref().unchecked_ref())?;

            Ok(on_keydown)
        }
    }

    fn setup_keyup_listener(
        window: &Window,
        world: &Rc<RefCell<World>>,
    ) -> JsResult<EventListener> {
        {
            let world = Rc::clone(world);

            let on_keyup = Closure::wrap(Box::new(move |event: Event| {
                let event: KeyboardEvent = event.unchecked_into();
                Game::on_key_up(world.borrow(), &event.code());
            }) as Box<dyn FnMut(Event)>);

            window.add_event_listener_with_callback("keyup", on_keyup.as_ref().unchecked_ref())?;

            Ok(on_keyup)
        }
    }

    fn setup_resize_listener(
        window: &Window,
        world: &Rc<RefCell<World>>,
    ) -> JsResult<EventListener> {
        let world = Rc::clone(world);

        let on_resize = Closure::wrap(Box::new(move |event: Event| {
            let target: Window = event.target().unwrap().unchecked_into();
            let width = target.inner_width().unwrap().as_f64().unwrap() as u32;
            let height = target.inner_height().unwrap().as_f64().unwrap() as u32;

            Game::on_resize(world.borrow(), width, height);
        }) as Box<dyn FnMut(Event)>);

        window.add_event_listener_with_callback("resize", on_resize.as_ref().unchecked_ref())?;

        Ok(on_resize)
    }

    fn setup_websocket(world: &Rc<RefCell<World>>) -> JsResult<(Rc<WebSocket>, EventListener)> {
        let websocket = Rc::new(WebSocket::new_with_str("ws://localhost:1991", "awsp")?);
        websocket.set_binary_type(BinaryType::Arraybuffer);

        let mut buffer = Vec::<u8>::new();

        let on_message = {
            let world = Rc::clone(world);
            let websocket = Rc::clone(&websocket);

            Closure::wrap(Box::new(move |event: Event| {
                let event: MessageEvent = event.unchecked_into();
                let data = event.data();

                if let Some(text) = data.as_string() {
                    Game::handle_text_message(&text, &websocket, &world);
                } else if let Some(arary_buffer) = data.dyn_ref::<ArrayBuffer>() {
                    let u8_array = Uint8Array::new(arary_buffer);

                    buffer.resize(u8_array.length() as usize, 0);
                    u8_array.copy_to(&mut buffer);

                    Game::handle_bin_message(&buffer, &websocket, &world);
                } else {
                    info!("MessageEvent data is of an unknown type.");
                }
            }) as Box<dyn FnMut(Event)>)
        };

        websocket
            .add_event_listener_with_callback("message", on_message.as_ref().unchecked_ref())?;

        Ok((websocket, on_message))
    }

    fn handle_text_message(text: &str, websocket: &WebSocket, world: &RefCell<World>) {
        if let Ok(message) = serde_json::from_str::<Message>(text) {
            debug!("Deserialized message: {:?}", message);

            match message {
                Message::Connected(_) => {
                    debug!("Received connected, starting spawn");
                    let spawn = Message::Spawn;
                    websocket
                        .send_with_str(&serde_json::to_string(&spawn).unwrap())
                        .unwrap();
                }
                msg => {
                    debug!("Received unhandled message, sending to system");
                    let world = world.borrow();
                    let mut message_chan = world.write_resource::<EventChannel<Message>>();
                    message_chan.single_write(msg);
                }
            }
        } else {
            info!("Received unknown message.");
        }
    }

    fn handle_bin_message(bytes: &[u8], _websocket: &WebSocket, world: &RefCell<World>) {
        if bytes[0] == 0 {
            let world = world.borrow();

            let mut state_updates = world.write_resource::<ReceivedServerStateUpdates>();
            state_updates.add(&bytes[1..]);
        }
    }
    fn on_key_down(world: Ref<World>, code: &str) {
        let mut input_handler = world.write_resource::<input::Handler>();
        input_handler.handle_event(&input::Event::KeyDown(map_key_code(code)));

        let mut input_events = world.write_resource::<EventChannel<input::Event>>();
        input_events.single_write(input::Event::KeyDown(map_key_code(code)));
    }

    fn on_key_up(world: Ref<World>, code: &str) {
        let mut input_handler = world.write_resource::<input::Handler>();
        input_handler.handle_event(&input::Event::KeyUp(map_key_code(code)));

        let mut input_events = world.write_resource::<EventChannel<input::Event>>();
        input_events.single_write(input::Event::KeyUp(map_key_code(code)));
    }

    fn on_resize(world: Ref<World>, width: u32, height: u32) {
        let mut resize_event_channel = world.write_resource::<EventChannel<ResizeEvent>>();
        resize_event_channel.single_write(ResizeEvent { width, height });
    }
}

#[wasm_bindgen]
impl Game {
    pub fn new(online: bool) -> JsResult<Game> {
        console_log::init_with_level(log::Level::Debug)
            .map_err(|err| format!("Failed to init console_log: {}", err))?;

        let window = web::window().ok_or("Could not access `window`")?;
        let crypto = window.crypto()?;

        let mut seed = [0u8; 16];
        crypto.get_random_values_with_u8_array(&mut seed)?;
        let seed = seed;

        let random = Random {
            rng: XorShiftRng::from_seed(seed),
        };

        let game_mode = if online {
            GameMode::Client
        } else {
            GameMode::Local
        };

        let mut world = affinity_game::create_world(random, game_mode);
        world.insert(EventChannel::<webgl::ResizeEvent>::new());

        let dispatcher_builder = affinity_game::create_dispatcher_builder(&world, game_mode)
            .with_thread_local(WebGlRenderer::default());

        let mut dispatcher = dispatcher_builder.build();

        dispatcher.setup(&mut world);

        let accumulator = DeltaTimeAccumulator::default();

        let world = Rc::new(RefCell::new(world));

        let mut event_listeners = Game::setup_event_listeners(&window, &world)?;

        let websocket = if online {
            let (websocket, on_message_listener) = Game::setup_websocket(&world)?;
            event_listeners.push(on_message_listener);
            Some(websocket)
        } else {
            None
        };

        Ok(Game {
            world,
            dispatcher,
            accumulator,
            event_listeners,
            buffer: Vec::new(),
            mode: game_mode,
            websocket,
        })
    }

    pub fn step(&mut self, deltatime_ms: f64) -> JsResult<()> {
        let deltatime = Self::get_deltatime_duration(deltatime_ms);
        self.accumulator.accumulate(deltatime);

        self.step_fixed(deltatime);

        // TODO: Fix systems being run twice. Need separate dispatchers?
        // self.step_loose(deltatime);

        self.sync_to_server()
    }

    fn get_deltatime_duration(deltatime_ms: f64) -> Duration {
        const DT_MAX: Duration = Duration::from_millis(250);

        let deltatime_s = deltatime_ms * 1.0e-3;
        let dt_secs = deltatime_s as u64;

        let deltatime_s_leftover = deltatime_s - (dt_secs as f64);
        let dt_nanos = (deltatime_s_leftover * 1.0e9) as u32;

        let mut dt_duration = Duration::new(dt_secs, dt_nanos);
        if dt_duration > DT_MAX {
            dt_duration = DT_MAX;
        }

        dt_duration
    }

    pub fn pop_asset_queue(&mut self) -> Option<String> {
        let world = self.world.borrow();

        let mut assets = world.write_resource::<Assets>();

        assets.take_queued_load().map(|name| name.into())
    }

    pub fn on_asset_fetched(&mut self, name: &str, data_view: Uint8Array) {
        info!("Asset {} loaded!", name);

        self.buffer.resize(data_view.length() as usize, 0);
        data_view.copy_to(&mut self.buffer);

        let asset = Asset {
            name: name.into(),
            data: self.buffer.clone(),
        };

        let world = self.world.borrow();

        let mut assets = world.write_resource::<Assets>();
        assets.finish_load(asset);
    }

    pub fn on_level_file_loaded(&mut self, data_view: Uint8Array) -> JsResult<()> {
        self.buffer.resize(data_view.length() as usize, 0);
        data_view.copy_to(&mut self.buffer);

        let mut world = self.world.borrow_mut();

        affinity_game::load_level(&mut world, &self.buffer, self.mode)
            .map_err(|err| format!("Failed loading level: {}", err))?;

        Ok(())
    }

    fn sync_to_server(&self) -> JsResult<()> {
        if let Some(websocket) = &self.websocket {
            if websocket.ready_state() == WebSocket::OPEN {
                let world = self.world.borrow();

                let mut state_update = world.write_resource::<ClientStateUpdate>();
                self.send_state_update(websocket, &mut state_update)?;
                state_update.clear();
            }
        }

        Ok(())
    }

    fn send_state_update(
        &self,
        websocket: &WebSocket,
        state_update: &mut ClientStateUpdate,
    ) -> JsResult<()> {
        let mut buffer = vec![1u8];
        buffer.append(&mut state_update.component_states_bytes);

        debug!("Sending state update of size {} to server", buffer.len());
        websocket.send_with_u8_array(&buffer)
    }
}

fn map_key_code(code: &str) -> Key {
    match code {
        "KeyW" => Key::W,
        "KeyS" => Key::S,
        "KeyD" => Key::D,
        "KeyA" => Key::A,
        "Escape" => Key::Escape,
        "Minus" => Key::Minus,
        "Equal" => Key::Equal,
        "Space" => Key::Space,
        _ => Key::Unknown,
    }
}
