extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;

#[proc_macro_derive(SystemName)]
pub fn system_name_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();

    impl_system_name_macro(&ast)
}

fn impl_system_name_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl SystemName for #name {
            const NAME: &'static str = stringify!(#name);
        }
    };
    gen.into()
}