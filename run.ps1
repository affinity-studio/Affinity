#!/usr/bin/pwsh

param (
    [string]$Task = "",
    [string]$Mode = "development"
)

$WebDir = "./web"
$RustChannel = "stable"
$MinRustVersion = "1.57.0"
$WantedWasmBindgenVersion = "0.2.81"

if (!(Test-Path -Path "$WebDir/wasm" )) {
    New-Item -ItemType directory -Path "$WebDir/wasm"
}

function Build-Wasm {
    if ($Mode -eq "production") {
        cargo +$RustChannel build --color always --package affinity_web --target wasm32-unknown-unknown --release 2>&1

        if (Get-Command wasm-opt -ErrorAction SilentlyContinue) {
            Write-Host "Optimizing wasm size"
            wasm-opt -Oz target/wasm32-unknown-unknown/release/affinity_web.wasm -o target/wasm32-unknown-unknown/release/affinity_web_opt.wasm
            $WasmPath = "target/wasm32-unknown-unknown/release/affinity_web_opt.wasm"
        }
        else {
            Write-Host "To get even smaller wasm, install wasm-opt and add it to the path (https://github.com/webassembly/binaryen)"
            $WasmPath = "target/wasm32-unknown-unknown/release/affinity_web.wasm"
        }
    }
    else {
        cargo +$RustChannel build --color always --package affinity_web --target wasm32-unknown-unknown 2>&1
        $WasmPath = "target/wasm32-unknown-unknown/debug/affinity_web.wasm"
    }

    Write-Host "Running wasm-bindgen"
    wasm-bindgen $WasmPath --out-dir $WebDir/wasm
}

function Build-Server {
    if ($Mode -eq "production") {
        cargo +$RustChannel build --color always --package affinity_server --release 2>&1
    }
    else {
        cargo +$RustChannel build --color always --package affinity_server 2>&1
    }
}

function Invoke-Server {
    if ($Mode -eq "production") {
        cargo +$RustChannel run --color always --package affinity_server --release 2>&1
    }
    else {
        cargo +$RustChannel run --color always --package affinity_server 2>&1
    }
}

function Build-Yarn {
    Push-Location ./web
    try {
        if ($Mode -eq "production") {
            yarn run build:prod
        }
        else {
            yarn run build:dev
        }
    }
    finally {
        Pop-Location
    }
}

function Initialize-Project {
    rustc +$RustChannel | Out-Null
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Installing rust $RustChannel"
        rustup toolchain install $RustChannel
    }

    $InstalledRustVersion = rustc --version | Select-String -Pattern "\d+\.\d+\.\d+" | ForEach-Object { $_.Matches.Groups[0].ToString() } | Select-Object -Last 1
    if ($InstalledRustVersion -lt $MinRustVersion) {
        Write-Host "Rust $RustChannel $MinRustVersion is required. You have $InstalledRustVersion. Updating..."
        rustup update $RustChannel
    }

    rustup target add wasm32-unknown-unknown --toolchain $RustChannel
    rustup component add clippy --toolchain $RustChannel

    $InstalledWasmBindgenVersion = wasm-bindgen --version | Select-String -Pattern "\d+\.\d+\.\d+" | ForEach-Object { $_.Matches.Groups[0].ToString() } | Select-Object -Last 1
    if ($InstalledWasmBindgenVersion -ne $WantedWasmBindgenVersion) {
        Write-Host "Installing wasm-bindgen"
        cargo +$RustChannel install --force --version $WantedWasmBindgenVersion wasm-bindgen-cli
    }

    try {
        watchexec --version | Out-Null
    }
    catch {
        Write-Host "Installing watchexec"
        cargo install watchexec
    }

    Push-Location ./web
    try {
        yarn
    }
    finally {
        Pop-Location
    }

    git submodule update --init --recursive
}

function Update-Dependencies {
    cargo update

    Push-Location ./web
    try {
        yarn upgrade
    }
    finally {
        Pop-Location
    }
}

function Remove-WebArtifacts {
    if (Test-Path -Path "$WebDir/dist") {
        Remove-Item -Recurse $WebDir/dist
    }
}

function Invoke-Lint {
    cargo +$RustChannel clippy

    Push-Location ./web
    try {
        yarn run lint
    }
    finally {
        Pop-Location
    }
}

function Build-All {
    Write-Host "Cleaning web artifacts"
    Remove-WebArtifacts

    Write-Host "Building server"
    Build-Server

    Write-Host "Building wasm"
    Build-Wasm

    Write-Host "Building web"
    Build-Yarn
}

function Clear-Build {
    if (Test-Path -Path "$WebDir/dist") {
        Write-Host "Removing $WebDir/dist"
        Remove-Item -Recurse "$WebDir/dist"
    }

    if (Test-Path -Path "$WebDir/wasm") {
        Write-Host "Removing $WebDir/wasm"
        Remove-Item -Recurse "$WebDir/wasm"
    }

    if (Test-Path -Path "$WebDir/generated" ) {
        Write-Host "Removing $WebDir/generated"
        Remove-Item -Recurse "$WebDir/generated"
    }

    if (Test-Path -Path "./target" ) {
        Write-Host "Removing contents in ./target"
        Remove-Item -Recurse ./target/* -Exclude rls -Force
    }

}

switch ($Task) {
    "init" {
        Initialize-Project
    }
    "update" {
        Update-Dependencies
    }
    "clean" {
        Clear-Build
    }
    "build:wasm" {
        Build-Wasm
    }
    "build:yarn" {
        Build-Yarn
    }
    "run:server" {
        Invoke-Server
    }
    "build" {
        Build-All
    }
    "lint" {
        Invoke-Lint
    }
    default {
        Write-Host "Unknown task '$Task'"
        exit 1
    }
}
