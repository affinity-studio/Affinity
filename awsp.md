# Affinity WebSocket Protocol (AWSP) (draft)

AWSP is the WebSocket subprotocol used to communicate between the Affinity game client and game server.

## Potential issues

* Message that does not arrive. Retry? Cannot skip update.

## TODO

* Updates from client. Only input?
* Consider changing Component States to list all the Components per Entity instead of Entities per Component. This could save space, since an SID is four times as large as the Component Type indicator.

## Versioning

The AWSP is versioned by one unsigned integer value, starting at 0, indicating compatibility.
If client and server do not have exactly matching versions, they are incompatible, and should not communicate.

## Connecting

A WebSocket connection is created from the client to the server on a specific port on the server (by default, 1991), with the subprotocol named "awsp".

## Binary diagrams

Throughout this document, binary representations will have diagrams in the form of fixed-width text.
Following is an explanation of their structure.

```
One byte, with X representing its contents.
┌───┐
│ X │
└───┘
```

```
Content X using two bytes. This can be repeated for more bytes.
┌───┬───┐
│   X   │
└───┴───┘
```

```
Content X with unspecified size.
┌─╌─┐
│ X │
└─╌─┘
```

```
Content X repeated N times.
┌───┐┄
│ X │
└───┘┄
```

## Message formats

The protocol is mainly binary-based for updates between client and server, but also uses JSON for other messages.
When the message is binary, the first byte always indicates the type of message, and the remaining bytes is the message content.
Each type of message will define their lengths in different ways.
The corresponding byte value is specified in each message type in this document.

```
T = Type of message
C = Message contents
┌───┬─╌─┐┄
│ T │ C │
└───┴─╌─┘┄
```

For JSON messages, the message type is specified as a string on a "type" field in the root JSON object, and the remaining fields are the message contents.
The corresponding string value is specified in each message type in this document.

## Entity Synchronization ID

Each Entity in the world, that should be synchronized between server and client, has an __Entity Synchronization ID__ (SID). This is an unsigned 32-bit integer, indicating a unique ID for an Entity in the servers representation of the world.
The clients and server must use this ID to map between the local Entities (on the client) and the remote Entities (on the server), and vice versa.

## Message types

### Server State Update

State Updates are binary and the type is indicated by the unsigned integer value 0 in the first byte.

The contents consists of multiple sections in a fixed order, each starting with two bytes indicating their length as an unsigned 16-bit integer.
The sections are as follows, in the same order:

1. Deleted Entities
2. Created Entities
3. Component States

```
D = Deleted Entities
C = Created Entities
S = Component States
L(X) = length of X in bytes
┌───┬───┬───┬─╌─┬───┬───┬─╌─┬───┬───┬─╌─┐
│ 0 │  L(D) │ D │  L(C) │ C │  L(S) │ S │
└───┴───┴───┴─╌─┴───┴───┴─╌─┴───┴───┴─╌─┘
```

#### Deleted Entities

This is a list of SIDs of Entities that the client should remove from the local world.
It is important that this is processed first, because SIDs may be reused for new Entities.
The client should completely remove the Entity mapped from this SID from their local world.

```
┌───┬───┬───┬───┐┄
│      SID      │
└───┴───┴───┴───┘┄
```

#### Created Entities

This is a list of SIDs of Entities that the client should create in the local world.
The client should create a new Entity for each of the SIDs, and map the SID to this Entity.

```
┌───┬───┬───┬───┐┄
│      SID      │
└───┴───┴───┴───┘┄
```

#### Component States

The __Component States__ section contains the state of all of the synchronized components of all of the entities available to the client.
What components are synchronized, and what entities are available, is out of scope for this specification.

The section is subdivided into __Component Type__ sections.
Each of these sections starts with a byte indicating the Component Type, and two bytes (16-bit unsigned integer) indicating the byte length of the section contents.
The remaining bytes is the list of serialized __Component Instance__.

```
T = Component Type
S = Component Type section
L(X) = length of X in bytes
┌───┬───┬───┬─╌─┐┄
│ T │  L(S) │ S │
└───┴───┴───┴─╌─┘┄
```

Each __Component Instance__ starts with the SID, and the serialized state of the Component Instance.
If the Component Instance does not exist on the local version of the Entity, a new instance of the Component type should be added to this Entity, and be updated with the state. Otherwise, the existing instance of the Component is updated with the state.
It is up to each component synchronizer to know how to serialize/deserialize this state (though bincode is recommended).

```
C = Serialized Component Instance
┌───┬───┬───┬───┬─╌─┐┄
│      SID      │ C │
└───┴───┴───┴───┴─╌─┘┄
```

### Client State Update

Client State Updates are binary and the type is indicated by the unsigned integer value 1 in the first byte.

These are similar to Server State Updates, but contain only Component States, and only for the controled entity.
It consists of the length of the Component States in bytes as a u16, and then the Component States.

```
S = Component States
L(X) = length of X in bytes
┌───┬───┬───┬─╌─┐
│ 1 │  L(S) │ S │
└───┴───┴───┴─╌─┘
```

The __Component States__ section contains the state of all of the reverse synchronized components of the client controlled entity.
What components are synchronized is out of scope for this specification.

The section is subdivided into __Component Type__ sections.
Each of these sections starts with a byte indicating the Component Type, and two bytes (16-bit unsigned integer) indicating the byte length of the section contents.
The remaining bytes is the serialized __Component Instance__.

```
T = Component Type
C = Serialized Component Instance
L(X) = length of X in bytes
┌───┬───┬───┬─╌─┐┄
│ T │  L(C) │ C │
└───┴───┴───┴─╌─┘┄
```

The __Component Instance__ is the serialized state of the Component.
If the Component Instance does not exist on the local version of the Entity, a new instance of the Component type should be added to this Entity, and be updated with the state. Otherwise, the existing instance of the Component is updated with the state.
It is up to each component synchronizer to know how to serialize/deserialize this state (though bincode is recommended).


### Events Update

WIP

Events Update are binary and the type is indicated by the unsigned integer value 1 in the first byte.
It contains all events that are synchronized from client to server.


```
E = Events
L(X) = length of X in bytes
┌───┬───┬───┬─╌─┐
│ 1 │  L(E) │ E │
└───┴───┴───┴─╌─┘
```

#### Events

The __Events__ section contains the data of the synchronized events.
What events are synchronized is out of scope for this specification.

The section is subdivided into __Event Type__ sections.
Each of these sections starts with a byte indicating the Event Type, and two bytes (16-bit unsigned integer) indicating the byte length of the section contents.
The remaining bytes is the list of serialized __Event Instance__.

```
T = Event Type
S = Event Type section contents
L(X) = length of X in bytes
┌───┬───┬───┬─╌─┐┄
│ T │  L(S) │ S │
└───┴───┴───┴─╌─┘┄
```

Each __Event Instance__ is just the serialized event.
It is up to each event synchronizer to know how to serialize/deserialize the data (though bincode is recommended).

```
C = Serialized Component Instance
┌───┬───┬───┬───┬─╌─┐┄
│      SID      │ C │
└───┴───┴───┴───┴─╌─┘┄
```

### Connected

Connected is a JSON message with type "connected".

This message is sent by the server to the client when a client is connected, and contains the assigned Client ID.

Properties:
```json
{
    "type": {
        "const": "connected"
    },
    "cid": {
        "description": "The Client ID assigned to this client",
        "type": "number"
    }
}
```

### Spawn Request

Spawn Request is a JSON message with type "spawn_request".

This message may be sent from client to server to request that the client can spawn as a player in the world.

Properties:
```json
{
    "type": {
        "const": "spawn"
    }
}
```

### Spawned

Spawned is a JSON message with type "spawned".

This message is sent by the server to the client when the client has been spawned as a player in the world (usually after a Spawn Request).

Properties:
```json
{
    "type": {
        "const": "spawned"
    },
    "sid": {
        "description": "The Entity Synchronization ID of the Entity that this client now controls",
        "type": "number"
    }
}
```