use alga::general::{Additive, Identity};
use nalgebra::{self as na, Scalar};
use std::ops::{Deref, Mul, MulAssign};

pub struct Kg<T: Scalar>(T);

impl<T: Scalar> Kg<T> {
    pub fn new(value: T) -> Kg<T> {
        Kg(value)
    }
}

impl<T> Default for Kg<T>
where
    T: Identity<Additive> + Scalar,
{
    fn default() -> Kg<T> {
        Kg(na::zero())
    }
}

impl<T: Scalar> Deref for Kg<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

#[derive(Clone, Copy)]
pub struct Mps2<T: Scalar>(T);

impl<T: Scalar> Mps2<T> {
    pub fn new(value: T) -> Mps2<T> {
        Mps2(value)
    }
}

impl<T> Default for Mps2<T>
where
    T: Identity<Additive> + Scalar,
{
    fn default() -> Mps2<T> {
        Mps2(na::zero())
    }
}

impl<T: Scalar> Deref for Mps2<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Mps<T: Scalar>(T);

impl<T: Scalar> Mps<T> {
    pub fn new(value: T) -> Mps<T> {
        Mps(value)
    }
}

impl<T> Default for Mps<T>
where
    T: Identity<Additive> + Scalar,
{
    fn default() -> Mps<T> {
        Mps(na::zero())
    }
}

impl<T: Scalar> Deref for Mps<T> {
    type Target = T;

    fn deref(&self) -> &T {
        &self.0
    }
}

impl<T> Mul<T> for Mps<T>
where
    T: Scalar + Mul<Output = T>,
{
    type Output = Mps<T>;

    fn mul(self, rhs: T) -> Mps<T> {
        Mps::new(self.0 * rhs)
    }
}

impl<T> MulAssign<T> for Mps<T>
where
    T: Scalar + MulAssign,
{
    fn mul_assign(&mut self, rhs: T) {
        self.0 *= rhs;
    }
}
