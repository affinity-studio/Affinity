use nalgebra::base::Scalar;
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
pub struct Rect<T: Scalar> {
    pub x: T,
    pub y: T,
    pub w: T,
    pub h: T,
}

impl<T: Scalar> Rect<T> {
    pub fn new(x: T, y: T, w: T, h: T) -> Rect<T> {
        Rect { x, y, w, h }
    }
}
