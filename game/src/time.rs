use std::time::Duration;
use crate::core::utils::duration_as_secs_f32;

/// Accumulates delta time, and then consumes it at a fixed step size.
/// 
/// Used for updates that require a stable step size.
pub struct DeltaTimeAccumulator {
    consumption_step: Duration,
    accumulated: Duration,
}

impl DeltaTimeAccumulator {
    /// Construct a new DeltaTimeAccumulator with a fixed consumption step
    pub fn new(consumption_step: Duration) -> DeltaTimeAccumulator {
        DeltaTimeAccumulator {
            accumulated: Duration::default(),
            consumption_step,
        }
    }

    /// Accumulate time that later can be consumer
    pub fn accumulate(&mut self, dt: Duration) {
        self.accumulated += dt;
    }

    /// Consume one fixed step, if enough time is accumulated
    /// 
    /// Returns a bool indicating if the step was consumed or not
    pub fn consume(&mut self) -> bool {
        if self.accumulated >= self.consumption_step {
            self.accumulated -= self.consumption_step;
            true
        } else {
            false
        }
    }

    /// Check if the accumulator has accumulated enough time to consume one fixed step
    pub fn is_consumable(&self) -> bool {
        self.accumulated >= self.consumption_step
    }

    pub fn consumption_step(&self) -> Duration {
        self.consumption_step
    }
    
    /// Get the alpha of the accumulated time, i.e. how far from the next fixed step the accumulator is, in a range of 0 to 1.
    pub fn alpha(&self, dt: Duration) -> f32 {
        duration_as_secs_f32(&self.accumulated) / duration_as_secs_f32(&dt)
    }

    /// Time that needs to be accumulated to be able to consume another step
    pub fn remaining(&self) -> Duration {
        if self.accumulated > self.consumption_step {
            Duration::from_secs(0)
        } else {
            self.consumption_step - self.accumulated
        }
    }
}

impl Default for DeltaTimeAccumulator {
    fn default() -> DeltaTimeAccumulator {
        DeltaTimeAccumulator::new(Duration::from_nanos((1.0 / 60.0 * 1.0e9) as u64))
    }
}