use nalgebra::clamp;
use serde_derive::{Deserialize, Serialize};
use specs::prelude::*;
use specs_derive::Component;

use crate::{
    core::ecs::SystemName,
    input::{self, Key},
    physics::{
        components::{ContactMonitor, Velocity},
        Contact,
    },
    resources::DeltaTime,
};
use affinity_derive::SystemName;

#[derive(Component, Default, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Control {
    walk_direction: f32,
    jumping: bool,
}

#[derive(SystemName, Default)]
pub struct ControlActuator;

impl ControlActuator {
    fn execute_controls(
        &self,
        control: &Control,
        contacts: &[Contact],
        velocity: &mut Velocity,
        dt: &DeltaTime,
    ) {
        let on_ground = Self::entity_is_on_ground(contacts);

        let horizontal_movement = Self::apply_horizontal_control(velocity.0.x, control, dt);
        let horizontal_movement = Self::apply_horizontal_friction(horizontal_movement, dt);

        let vertical_movement = Self::apply_jump(velocity.0.y, on_ground, control);

        velocity.0.x = horizontal_movement;
        velocity.0.y = vertical_movement;
    }

    fn apply_horizontal_control(current_speed: f32, control: &Control, dt: &DeltaTime) -> f32 {
        const MOVEMENT_SPEED: f32 = 5.0;
        const MOVEMENT_ACCEL: f32 = 120.0;

        let walk_direction = clamp(control.walk_direction, -1.0, 1.0);
        let updated_speed = current_speed + walk_direction * MOVEMENT_ACCEL * dt.multiplier();

        clamp(updated_speed, -MOVEMENT_SPEED, MOVEMENT_SPEED)
    }

    fn apply_horizontal_friction(current_speed: f32, dt: &DeltaTime) -> f32 {
        const MOVEMENT_FRICTION: f32 = 30.0;

        // apply friction to velocity
        let updated_speed = if current_speed > 0.1 {
            current_speed - MOVEMENT_FRICTION * dt.multiplier()
        } else if current_speed < -0.1 {
            current_speed + MOVEMENT_FRICTION * dt.multiplier()
        } else {
            current_speed
        };

        // stop horizontal movement if speed is low
        if updated_speed.abs() < 0.1 {
            0.0
        } else {
            updated_speed
        }
    }

    fn apply_jump(current_speed: f32, on_ground: bool, control: &Control) -> f32 {
        const JUMP_SPEED: f32 = 5.0;

        if control.jumping && on_ground {
            current_speed.max(JUMP_SPEED)
        } else {
            current_speed
        }
    }

    fn entity_is_on_ground(contacts: &[Contact]) -> bool {
        for contact in contacts {
            if contact.normal.y >= contact.normal.x.abs() {
                return true;
            }
        }

        false
    }
}

impl<'a> System<'a> for ControlActuator {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        ReadStorage<'a, Control>,
        ReadStorage<'a, ContactMonitor>,
        WriteStorage<'a, Velocity>,
        Read<'a, DeltaTime>,
    );

    fn run(&mut self, (controls, contact_monitors, mut velocities, dt): Self::SystemData) {
        for (control, contact_monitor, velocity) in
            (&controls, &contact_monitors, &mut velocities).join()
        {
            self.execute_controls(control, &contact_monitor.contacts, velocity, &dt);
        }
    }
}

#[derive(SystemName)]
pub struct PlayerInputController;

impl PlayerInputController {
    pub fn new(_world: &World) -> PlayerInputController {
        PlayerInputController {}
    }

    fn update_walk_direction(&self, input_handler: &input::Handler, control: &mut Control) {
        control.walk_direction = 0.0;

        if input_handler.key_pressed(Key::A) {
            control.walk_direction -= 1.0;
        }

        if input_handler.key_pressed(Key::D) {
            control.walk_direction += 1.0;
        }
    }

    fn update_jump(&self, input_handler: &input::Handler, control: &mut Control) {
        control.jumping = input_handler.key_pressed(Key::Space);
    }
}

impl<'a> System<'a> for PlayerInputController {
    #[allow(clippy::type_complexity)]
    type SystemData = (WriteStorage<'a, Control>, Read<'a, input::Handler>);

    fn run(&mut self, (mut controls, input_handler): Self::SystemData) {
        for control in (&mut controls).join() {
            self.update_walk_direction(&input_handler, control);
            self.update_jump(&input_handler, control);
        }
    }
}
