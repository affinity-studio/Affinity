use std::time::Duration;

pub fn duration_as_secs_f64(duration: &Duration) -> f64 {
    duration.as_nanos() as f64 * 1.0e-9
}

pub fn duration_as_secs_f32(duration: &Duration) -> f32 {
    duration_as_secs_f64(duration) as f32
}
