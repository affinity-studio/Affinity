pub trait SystemName {
    const NAME: &'static str;
}