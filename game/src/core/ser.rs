use nalgebra::{Scalar, Vector2};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
struct Point<T: Scalar> {
    x: T,
    y: T,
}

pub fn deserialize_vec2<'de, D, T>(deserializer: D) -> Result<Vector2<T>, D::Error>
where
    D: Deserializer<'de>,
    T: Scalar + Deserialize<'de>,
{
    let point: Point<T> = Deserialize::deserialize(deserializer)?;

    Ok(Vector2::new(point.x, point.y))
}

pub fn serialize_vec2<S, T>(vec: &Vector2<T>, seriailzer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
    T: Scalar + Serialize + Copy,
{
    let point = Point { x: vec.x, y: vec.y };

    Serialize::serialize(&point, seriailzer)
}
