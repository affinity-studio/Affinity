use specs::{Entity, Read, ReadExpect, ReadStorage, System, WriteStorage};

use crate::components::{Position, Scale, Target};
use crate::core::ecs::SystemName;
use crate::input::{self, Key};
use crate::resources::DeltaTime;

pub struct ActiveCamera(pub Entity);

#[derive(Default)]
pub struct CameraMovement;

const ZOOM_SPEED: f32 = 0.8;
const PAN_SPEED: f32 = 20.0;

impl<'a> System<'a> for CameraMovement {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteStorage<'a, Position>,
        WriteStorage<'a, Scale>,
        ReadStorage<'a, Target>,
        ReadExpect<'a, ActiveCamera>,
        Read<'a, input::Handler>,
        Read<'a, DeltaTime>,
    );

    fn run(
        &mut self,
        (mut positions, mut scales, targets, active_camera, input_handler, dt): Self::SystemData,
    ) {
        if let Some(target_entity) = targets.get(active_camera.0) {
            self.follow_entity(active_camera.0, target_entity.0, &mut positions);
        } else {
            self.move_freely(
                active_camera.0,
                &mut positions,
                &mut scales,
                &input_handler,
                &dt,
            );
        }
    }
}

impl CameraMovement {
    fn move_freely(
        &self,
        camera: Entity,
        positions: &mut WriteStorage<Position>,
        scales: &mut WriteStorage<Scale>,
        input_handler: &Read<input::Handler>,
        dt: &Read<DeltaTime>,
    ) {
        let mut pan_multiplier = PAN_SPEED * dt.multiplier();

        if let Some(scale) = scales.get_mut(camera) {
            if input_handler.key_pressed(Key::Equal) {
                scale.0 *= 1.0 + ZOOM_SPEED * dt.multiplier();
            }

            if input_handler.key_pressed(Key::Minus) {
                scale.0 *= 1.0 - ZOOM_SPEED * dt.multiplier();
            }

            pan_multiplier /= (scale.0.x + scale.0.y) / 2.0;
        }

        let pan_speed = pan_multiplier;

        if let Some(position) = positions.get_mut(camera) {
            if input_handler.key_pressed(Key::W) {
                position.0.y += pan_speed;
            }

            if input_handler.key_pressed(Key::S) {
                position.0.y -= pan_speed;
            }

            if input_handler.key_pressed(Key::A) {
                position.0.x -= pan_speed;
            }

            if input_handler.key_pressed(Key::D) {
                position.0.x += pan_speed;
            }
        }
    }

    fn follow_entity(
        &self,
        camera: Entity,
        target: Entity,
        positions: &mut WriteStorage<Position>,
    ) {
        if let Some(target_position) = positions.get(target) {
            let target_position = target_position.0;

            if let Some(position) = positions.get_mut(camera) {
                position.0 = target_position;
            }
        }
    }
}

impl SystemName for CameraMovement {
    const NAME: &'static str = "camera_movement";
}
