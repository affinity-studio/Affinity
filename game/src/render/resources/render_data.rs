pub struct RenderData {
    pub positions: Vec<f32>,
    pub instance_data: Vec<f32>,
    pub num_instances: usize,
}

impl RenderData {
    pub fn new() -> RenderData {
        Default::default()
    }
}

impl Default for RenderData {
    fn default() -> RenderData {
        RenderData {
            positions: vec![-0.5, -0.5, 0.5, -0.5, 0.5, 0.5, -0.5, -0.5, -0.5, 0.5],
            instance_data: vec![],
            num_instances: 0,
        }
    }
}
