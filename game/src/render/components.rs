pub use self::texture::{LoadedTexture, LoadingTexture, Texture};

pub mod texture {
    use nalgebra::Vector2;
    use serde_derive::{Deserialize, Serialize};
    use specs::{Component, VecStorage};
    use specs_derive::Component;

    use crate::assets::Handle;
    use crate::geom::Rect;

    #[derive(Component, Clone)]
    #[storage(VecStorage)]
    pub struct LoadedTexture {
        pub index: usize,
        pub offset: Vector2<u32>,
        pub size: Vector2<u32>,
        pub origin: Vector2<i32>,
    }

    #[derive(Component)]
    #[storage(VecStorage)]
    pub struct LoadingTexture {
        pub handle: Handle,
        pub section: Option<Section>,
    }

    #[derive(Component, Serialize, Deserialize)]
    #[storage(VecStorage)]
    pub enum Texture {
        Named(String),
        Cutout { source: String, section: Section },
    }

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct Section {
        pub area: Rect<u32>,
        #[serde(serialize_with = "crate::core::ser::serialize_vec2")]
        #[serde(deserialize_with = "crate::core::ser::deserialize_vec2")]
        pub offset: Vector2<i32>,
    }

    impl Texture {
        pub fn named(name: String) -> Texture {
            Texture::Named(name)
        }

        pub fn cutout(source: String, section: Section) -> Texture {
            Texture::Cutout { source, section }
        }
    }

    impl LoadedTexture {
        pub fn new() -> LoadedTexture {
            LoadedTexture {
                index: 0,
                offset: Vector2::new(0, 0),
                size: Vector2::new(0, 0),
                origin: Vector2::new(0, 0),
            }
        }
    }

    impl Default for LoadedTexture {
        fn default() -> LoadedTexture {
            Self::new()
        }
    }
}
