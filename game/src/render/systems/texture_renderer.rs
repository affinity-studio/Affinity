use specs::{ReadStorage, System, Write, WriteStorage};

use crate::components::{Orientation, Position, Scale};
use crate::core::ecs::SystemName;
use crate::render::{resources::RenderData, components::Texture, components::texture::AssetRef};
use crate::assets::Assets;

pub struct TextureRenderer;

impl<'a> System<'a> for TextureRenderer {
    type SystemData = (
        ReadStorage<'a, Position>,
        ReadStorage<'a, Orientation>,
        ReadStorage<'a, Scale>,
        WriteStorage<'a, Texture>,
        Write<'a, RenderData>,
        Write<'a, Assets>
    );

    fn run(&mut self, (positions, orientations, scales, mut textures, mut render_data, mut assets): Self::SystemData) {
        render_data.instance_data.clear();
        render_data.num_instances = 0;

        for (position, orientation, scale, texture) in (&positions, &orientations, &scales, &mut textures).join() {
            let asset_handle = match texture.asset {
                AssetRef::Handle(ref handle) => handle,
                AssetRef::Name(ref name) => {
                    let handle = if assets.needs_loading(name) {
                        assets.load(name)
                    } else {
                        assets.get_handle(name).unwrap()
                    };

                    texture.asset = AssetRef::Handle(handle);
                    texture.asset.as_handle().unwrap()
                }
            };


            // TODO: Put asset handle on another vector in RenderData.

            render_data.instance_data.reserve(5);
            render_data
                .instance_data
                .extend_from_slice(position.0.data.as_slice());
            render_data
                .instance_data
                .extend_from_slice(scale.0.data.as_slice());
            render_data.instance_data.push(orientation.0);
            render_data.num_instances += 1;
        }
    }
}

impl SystemName for TextureRenderer {
    const NAME: &'static str = "texture_renderer";
}
