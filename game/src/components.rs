use nalgebra::Vector2;
use serde_derive::{Deserialize, Serialize};
use specs::{Component, Entity, HashMapStorage, VecStorage};
use specs_derive::Component;
use std::time::Duration;

#[derive(Component, Clone, Copy, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Position(
    #[serde(serialize_with = "crate::core::ser::serialize_vec2")]
    #[serde(deserialize_with = "crate::core::ser::deserialize_vec2")]
    pub Vector2<f32>,
);

#[derive(Component, Clone, Copy, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Orientation(pub f32);

#[derive(Component, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Scale(
    #[serde(serialize_with = "crate::core::ser::serialize_vec2")]
    #[serde(deserialize_with = "crate::core::ser::deserialize_vec2")]
    pub Vector2<f32>,
);

#[derive(Component)]
#[storage(HashMapStorage)]
pub struct EmitterState {
    pub cooldown: Duration,
    pub time_since_previous: Duration,
}

impl EmitterState {
    pub fn new(rate: u32) -> EmitterState {
        let cooldown = Duration::from_nanos((1.0 / f64::from(rate) * 1.0e9) as u64);

        EmitterState {
            cooldown,
            time_since_previous: cooldown,
        }
    }
}

impl Orientation {
    pub fn as_direction(self) -> Vector2<f32> {
        Vector2::new(self.0.cos(), self.0.sin())
    }
}

impl Default for Scale {
    fn default() -> Scale {
        Scale(Vector2::new(1.0, 1.0))
    }
}

impl Default for Orientation {
    fn default() -> Orientation {
        Orientation(0.0)
    }
}

#[derive(Component)]
#[storage(HashMapStorage)]
pub struct Target(pub Entity);
