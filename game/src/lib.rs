use control::PlayerInputController;
use failure::Error;
use nalgebra::Vector2;
use ncollide2d::shape::{self, ShapeHandle};
use shrev::EventChannel;
use specs::prelude::*;
use std::marker::PhantomData;

use crate::{
    assets::Assets,
    camera::{ActiveCamera, CameraMovement},
    components::*,
    control::{Control, ControlActuator},
    core::ecs::system_name::SystemName,
    physics::{components::*, systems::*, CollisionObjectBehavior, ContactEvent},
    render::{
        components::{LoadedTexture, LoadingTexture, Texture},
        resources::RenderData,
    },
    resources::{DeltaTime, Random},
    systems::*,
};

#[cfg(feature = "net")]
use crate::net::{SyncId, Synced};

#[cfg(feature = "server")]
use crate::net::server::{
    self,
    reader::WithServerComponentReader,
    received_state_update::ReceviedClientStateUpdateResetter,
    writer::{Serialized, WithServerComponentWriter},
    MessageToClient, SyncIdAllocator,
};

#[cfg(feature = "client")]
use crate::net::{
    client::{self, reader::WithClientComponentReader, writer::WithClientComponentWriter},
    Message,
};

pub mod assets;
pub mod camera;
pub mod components;
pub mod control;
pub mod core;
pub mod geom;
pub mod input;
pub mod physics;
pub mod render;
pub mod resources;
pub mod ser;
pub mod systems;
pub mod time;

#[cfg(feature = "net")]
pub mod net;

pub const TILE_SIZE: u32 = 32;

#[derive(Default)]
struct ComponentIdCounter(u8);

pub struct ComponentId<T> {
    pub id: u8,
    _marker: PhantomData<T>,
}

impl<T> ComponentId<T> {
    fn new(id: u8) -> ComponentId<T> {
        ComponentId {
            id,
            _marker: Default::default(),
        }
    }
}

// TODO: Consider a different approach that does not rely on sequential IDS?

/// Register a new component in the world and with a sequential ID
///
/// The order in which this function is called with different
/// `Component`s is crucial for the ID they are assigned.
/// Changing the order will chaing their IDs
fn register_component<T>(world: &mut World)
where
    T: Component + Sync + Send,
    T::Storage: Default,
{
    world.register::<T>();

    let id = {
        let mut counter = world
            .entry::<ComponentIdCounter>()
            .or_insert_with(ComponentIdCounter::default);
        let id = counter.0;
        counter.0 += 1;
        id
    };

    world.insert(ComponentId::<T>::new(id))
}

fn register_synced_component<T>(world: &mut World)
where
    T: Component + Sync + Send,
    T::Storage: Default,
{
    register_component::<T>(world);

    #[cfg(feature = "server")]
    {
        world.insert(Serialized::<T>::default());
    }
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum GameMode {
    Local,
    Server,
    Client,
}

pub fn create_world(random: Random, mode: GameMode) -> World {
    let mut world = World::new();

    register_synced_component::<Position>(&mut world);
    register_synced_component::<Orientation>(&mut world);
    register_synced_component::<Scale>(&mut world);
    register_synced_component::<Velocity>(&mut world);
    register_component::<AngularVelocity>(&mut world);
    register_component::<EmitterState>(&mut world);
    register_synced_component::<Texture>(&mut world);
    register_component::<Acceleration>(&mut world);
    register_component::<Forces>(&mut world);
    register_component::<Mass>(&mut world);
    register_component::<CollisionObjectDescription>(&mut world);
    register_component::<CollisionObject>(&mut world);
    register_component::<Target>(&mut world);
    register_synced_component::<ContactMonitor>(&mut world);
    register_component::<Control>(&mut world);

    world.insert(DeltaTime::new());
    world.insert(random);
    world.insert(input::Handler::new());
    world.insert(EventChannel::<input::Event>::new());
    world.insert(EventChannel::<ContactEvent>::new());

    #[cfg(feature = "net")]
    {
        register_component::<Synced>(&mut world);
        register_component::<SyncId>(&mut world);
    }

    #[cfg(feature = "server")]
    {
        world.insert(SyncIdAllocator::default());
        world.insert(EventChannel::<server::NetworkEvent>::new());
        world.insert(EventChannel::<MessageToClient>::new());
        world.insert(server::ServerStateUpdates::default());
        world.insert(server::Clients::default());
        world.insert(server::received_state_update::ReceivedClientStateUpdates::default());
    }

    #[cfg(feature = "client")]
    {
        world.insert(EventChannel::<Message>::new());
        world.insert(client::ReceivedServerStateUpdates::default());
        world.insert(client::EntitySyncIdMap::default());
    }

    if mode != GameMode::Server {
        register_component::<LoadedTexture>(&mut world);
        register_component::<LoadingTexture>(&mut world);

        world.insert(RenderData::new());
        world.insert(Assets::default());
        world.insert(EventChannel::<input::Event>::new());

        let camera = world
            .create_entity()
            .with(Position(Vector2::new(0.0, 0.0)))
            .with(Orientation::default())
            .with(Scale(Vector2::new(3.0, 3.0)))
            .build();
        world.insert(ActiveCamera(camera));
    }

    world
}

pub fn create_dispatcher_builder<'a, 'b>(
    world: &World,
    mode: GameMode,
) -> DispatcherBuilder<'a, 'b> {
    let mut builder = DispatcherBuilder::new();

    // TODO: Consider moving networked state udateds to separate dispatcher in order to remove reduntant processing

    #[cfg(feature = "client")]
    {
        builder = builder
            .with(client::Synchronizer, client::Synchronizer::NAME, &[])
            .with(
                client::ClientStateUpdateResetter,
                client::ClientStateUpdateResetter::NAME,
                &[],
            )
            .with_barrier()
            .with_client_component_reader::<Position>("Position")
            .with_client_component_reader::<Orientation>("Orientation")
            .with_client_component_reader::<Scale>("Scale")
            .with_client_component_reader::<Texture>("Texture")
            .with_client_component_reader::<Velocity>("Velocity")
            .with_client_null_component_reader::<ContactMonitor>("ContactMonitor")
            .with_barrier();
    }

    #[cfg(feature = "server")]
    {
        builder = builder
            .with_server_component_reader::<Control>("Control")
            .with_barrier();
    }

    if mode != GameMode::Server {
        builder = builder
            .with(
                PlayerInputController::new(world),
                PlayerInputController::NAME,
                &[],
            )
            .with(
                ControlActuator::default(),
                ControlActuator::NAME,
                &[PlayerInputController::NAME],
            );
    } else {
        builder = builder.with(ControlActuator::default(), ControlActuator::NAME, &[]);
    }

    builder = builder
        .with_barrier()
        .with(UniversalGravitation, UniversalGravitation::NAME, &[])
        .with(
            ForceApplier,
            ForceApplier::NAME,
            &[UniversalGravitation::NAME],
        )
        .with(Accelerator, Accelerator::NAME, &[ForceApplier::NAME])
        .with(VelocityApplier, VelocityApplier::NAME, &[Accelerator::NAME])
        .with(
            AngularVelocityApplier,
            AngularVelocityApplier::NAME,
            &[Accelerator::NAME],
        )
        .with(
            Collision::default(),
            Collision::NAME,
            &[VelocityApplier::NAME, AngularVelocityApplier::NAME],
        )
        .with(Emitter, Emitter::NAME, &[]);

    if mode != GameMode::Server {
        builder = builder.with(CameraMovement, CameraMovement::NAME, &[Collision::NAME])
    }

    #[cfg(feature = "server")]
    {
        builder = builder
            .with(
                server::ClientManager::new(world),
                server::ClientManager::NAME,
                &[],
            )
            .with_barrier()
            .with(
                server::Synchronizer::new(world),
                server::Synchronizer::NAME,
                &[],
            )
            .with_server_component_writer::<Position>("Position")
            .with_server_component_writer::<Orientation>("Orientation")
            .with_server_component_writer::<Scale>("Scale")
            .with_server_component_writer::<Texture>("Texture")
            .with_server_component_writer::<Velocity>("Velocity")
            .with_server_null_component_writer::<ContactMonitor>("ContactMonitor")
            .with(
                ReceviedClientStateUpdateResetter,
                ReceviedClientStateUpdateResetter::NAME,
                &[],
            );
    }

    #[cfg(feature = "client")]
    {
        builder = builder
            .with_barrier()
            .with(
                client::PlayerEntityController::new(world),
                client::PlayerEntityController::NAME,
                &[],
            )
            .with_client_component_writer::<Control>("Control", &[])
            .with(
                client::ReceviedServerStateUpdateResetter,
                client::ReceviedServerStateUpdateResetter::NAME,
                &[],
            );
    }

    builder
}

pub fn load_level(world: &mut World, level_bytes: &[u8], mode: GameMode) -> Result<(), Error> {
    let level: level::Level = serde_json::from_slice(level_bytes)?;

    for tile in &level.tiles {
        world
            .create_entity()
            .with(Synced)
            .with(Position(Vector2::new(
                tile.x as f32 / TILE_SIZE as f32,
                -tile.y as f32 / TILE_SIZE as f32,
            )))
            .with(Orientation::default())
            .with(Scale::default())
            .with(Mass::new(1000.0))
            .with(CollisionObjectDescription {
                behavior: CollisionObjectBehavior::Static,
                shape: ShapeHandle::new(shape::Cuboid::new(Vector2::new(0.5, 0.5))),
            })
            .with(Texture::named(tile.name.clone()))
            .build();
    }

    if mode == GameMode::Local {
        let player = world
            .create_entity()
            .with(Position(Vector2::new(0.0, 5.0)))
            .with(Orientation::default())
            .with(Scale::default())
            .with(Velocity::default())
            .with(Acceleration::default())
            .with(Forces::default())
            .with(Mass::new(70.0))
            .with(CollisionObjectDescription {
                behavior: CollisionObjectBehavior::Dynamic,
                shape: ShapeHandle::new(shape::Cuboid::new(Vector2::new(0.2, 0.5))),
            })
            .with(Texture::named("torch".to_string()))
            .with(ContactMonitor::default())
            .with(Control::default())
            .build();

        let active_camera = world.read_resource::<ActiveCamera>();
        let camera_entity = active_camera.0;

        let mut targets = world.write_storage::<Target>();
        targets.insert(camera_entity, Target(player))?;
    }

    Ok(())
}

mod level {
    use serde_derive::Deserialize;

    #[derive(Deserialize, Debug)]
    pub struct Level {
        pub tiles: Vec<Tile>,
    }

    #[derive(Deserialize, Debug)]
    pub struct Tile {
        pub name: String,
        pub x: i32,
        pub y: i32,
    }
}
