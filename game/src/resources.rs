use rand::prelude::*;
use rand_xorshift::XorShiftRng;
use std::time::Duration;
use crate::core::utils::duration_as_secs_f32;

#[derive(Default)]
pub struct DeltaTime {
    pub actual: Duration,
    pub fixed: Option<Duration>,
}

pub struct Random {
    pub rng: XorShiftRng,
}

impl DeltaTime {
    pub fn new() -> DeltaTime {
        Default::default()
    }

    #[inline]
    pub fn multiplier(&self) -> f32 {
        duration_as_secs_f32(&self.actual)
    }

    #[inline]
    pub fn fixed_multiplier(&self) -> Option<f32> {
        self.fixed.as_ref().map(duration_as_secs_f32)
    }
}

impl Random {
    pub fn new() -> Random {
        Default::default()
    }
}

impl Default for Random {
    fn default() -> Random {
        Random {
            rng: XorShiftRng::from_entropy(),
        }
    }
}
