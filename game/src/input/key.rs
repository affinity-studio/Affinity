#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum Key {
    W,
    A,
    S,
    D,
    Up,
    Down,
    Left,
    Right,
    Escape,
    Minus,
    Equal,
    Space,
    Unknown,
}