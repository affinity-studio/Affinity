use super::Event;
use super::Key;
use hashbrown::HashSet;

#[derive(Default)]
pub struct Handler {
    pressed_keys: HashSet<Key>,
}

impl Handler {
    pub fn new() -> Handler {
        Default::default()
    }

    pub fn handle_event(&mut self, event: &Event) {
        match event {
            Event::KeyDown(key) => {
                self.pressed_keys.insert(*key);
            }
            Event::KeyUp(key) => {
                self.pressed_keys.remove(key);
            }
        }
    }

    pub fn key_pressed(&self, key: Key) -> bool {
        self.pressed_keys.contains(&key)
    }
}
