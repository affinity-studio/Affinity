use super::Key;

pub enum Event {
    KeyDown(Key),
    KeyUp(Key),
}