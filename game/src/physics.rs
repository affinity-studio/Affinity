use nalgebra::{Point2, Unit, Vector2};
use specs::Entity;

pub mod components;
pub mod resources;
pub mod systems;

#[derive(PartialEq)]
pub enum CollisionObjectBehavior {
    Static,
    Dynamic,
}

pub struct Contact {
    pub world1: Point2<f32>,
    pub world2: Point2<f32>,
    pub normal: Unit<Vector2<f32>>,
}

pub struct BeginContact {
    pub a: Entity,
    pub b: Entity,
    pub contacts: Vec<Contact>,
}

pub struct EndContact {
    pub a: Entity,
    pub b: Entity,
}

pub enum ContactEvent {
    BeginContact(BeginContact),
    EndContact(EndContact),
}
