use specs::prelude::*;
use specs_derive::Component;

pub mod protocol;
pub mod component_update;

#[cfg(feature = "server")]
pub mod server;

#[cfg(feature = "client")]
pub mod client;

pub use protocol::{msg, Message};

/// Entity Synchronization ID that is used to sync Entities over the network
#[derive(Component)]
pub struct SyncId(pub u32);

/// Marker Component that indicates that the Entity should be synced over the network
pub struct Synced;

impl Component for Synced {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}
