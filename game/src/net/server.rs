use super::{msg, Message, SyncId, Synced};
use crate::components::Orientation;
use crate::components::Position;
use crate::components::Scale;
use crate::core::ecs::SystemName;
use crate::physics::components::Acceleration;
use crate::physics::components::CollisionObjectDescription;
use crate::physics::components::Forces;
use crate::physics::components::Mass;
use crate::physics::components::{ContactMonitor, Velocity};
use crate::physics::CollisionObjectBehavior;
use crate::render::components::texture::Texture;
use affinity_derive::SystemName;
use byteorder::{NetworkEndian, WriteBytesExt};
use hashbrown::HashMap;
use log::{debug, info};
use nalgebra::Vector2;
use ncollide2d::shape;
use ncollide2d::shape::ShapeHandle;
use shrev::EventChannel;
use specs::{prelude::*, world::Index};

pub mod reader;
pub mod received_state_update;
pub mod writer;

/// Resource that allocates `SyncId`
#[derive(Default)]
pub struct SyncIdAllocator {
    id_counter: u32,
    freed_ids: Vec<u32>,
    allocated: HashMap<Index, u32>,
}

impl SyncIdAllocator {
    pub fn allocate(&mut self, entity_index: Index) -> SyncId {
        let id = if let Some(id) = self.freed_ids.pop() {
            id
        } else {
            let id = self.id_counter;
            self.id_counter += 1;
            id
        };

        self.allocated.insert(entity_index, id);

        SyncId(id)
    }

    pub fn deallocate(&mut self, entity_index: Index) -> Option<u32> {
        if let Some(id) = self.allocated.remove(&entity_index) {
            self.freed_ids.push(id);
            Some(id)
        } else {
            None
        }
    }
}

#[derive(Default)]
pub struct ServerStateUpdate {
    finalized: Vec<u8>,
    deleted_entities: Vec<u8>,
    created_entities: Vec<u8>,
    component_states: Vec<u8>,
}

impl ServerStateUpdate {
    pub fn finalize(&mut self) -> Option<&[u8]> {
        self.finalized.clear();

        if self.deleted_entities.is_empty()
            && self.created_entities.is_empty()
            && self.component_states.is_empty()
        {
            return None;
        }

        self.finalized
            .write_u16::<NetworkEndian>(self.deleted_entities.len() as u16)
            .unwrap();
        self.finalized.append(&mut self.deleted_entities);

        self.finalized
            .write_u16::<NetworkEndian>(self.created_entities.len() as u16)
            .unwrap();
        self.finalized.append(&mut self.created_entities);

        self.finalized
            .write_u16::<NetworkEndian>(self.component_states.len() as u16)
            .unwrap();
        self.finalized.append(&mut self.component_states);

        Some(&self.finalized)
    }

    pub fn add_deleted_entity(&mut self, sid: u32) {
        self.deleted_entities
            .write_u32::<NetworkEndian>(sid)
            .unwrap();
    }

    pub fn add_created_entity(&mut self, sid: u32) {
        self.created_entities
            .write_u32::<NetworkEndian>(sid)
            .unwrap();
    }

    pub fn set_component_states(&mut self, componend_id: u8, bytes: &[u8]) {
        // debug!("Component states: {} {}", componend_id, bytes.len());
        self.component_states.push(componend_id);
        self.component_states
            .write_u16::<NetworkEndian>(bytes.len() as u16)
            .unwrap();
        self.component_states.extend_from_slice(bytes);
    }
}

#[derive(Default)]
pub struct ServerStateUpdates {
    pub client_updates: HashMap<u32, ServerStateUpdate>,
}

#[derive(Default)]
pub struct Clients {
    pub clients: Vec<u32>,
    client_entities: HashMap<u32, Entity>,
}

/// System that manages the core synchronization of Entities with the Synced component.
pub struct Synchronizer {
    added: BitSet,
    removed: BitSet,
    synced_component_event_reader_id: ReaderId<ComponentEvent>,
}

impl Synchronizer {
    pub fn new(world: &World) -> Synchronizer {
        Synchronizer {
            added: Default::default(),
            removed: Default::default(),
            synced_component_event_reader_id: WriteStorage::<Synced>::fetch(world)
                .register_reader(),
        }
    }
}

impl<'a> System<'a> for Synchronizer {
    type SystemData = (
        ReadStorage<'a, Synced>,
        WriteStorage<'a, SyncId>,
        Entities<'a>,
        Write<'a, SyncIdAllocator>,
        Write<'a, ServerStateUpdates>,
    );

    fn run(
        &mut self,
        (synced, mut sync_ids, entities, mut sync_id_allocator, mut state_updates): Self::SystemData,
    ) {
        self.added.clear();
        self.removed.clear();

        let synced_component_events = synced
            .channel()
            .read(&mut self.synced_component_event_reader_id);

        for event in synced_component_events {
            match *event {
                ComponentEvent::Inserted(id) => {
                    self.added.add(id);
                }
                ComponentEvent::Removed(id) => {
                    self.removed.add(id);
                }
                ComponentEvent::Modified(_) => {}
            }
        }

        for id in &self.removed {
            debug!("Removing id {}", id);
            let sid = sync_id_allocator.deallocate(id).unwrap();

            for update in state_updates.client_updates.values_mut() {
                update.add_deleted_entity(sid);
            }
        }

        for (entity, _) in (&entities, &self.added).join() {
            let sid = if let Some(sync_id) = sync_ids.get(entity) {
                sync_id.0
            } else {
                let sync_id = sync_id_allocator.allocate(entity.id());
                let sid = sync_id.0;
                sync_ids.insert(entity, sync_id).unwrap();

                sid
            };

            for update in state_updates.client_updates.values_mut() {
                update.add_created_entity(sid);
            }

            debug!("Added Sync {}", sid);
        }
    }
}

impl SystemName for Synchronizer {
    const NAME: &'static str = "Synchronizer";
}

/// System that spawns client controlled Entities.
#[derive(SystemName)]
pub struct ClientManager {
    event_reader_id: ReaderId<NetworkEvent>,
}

impl ClientManager {
    pub fn new(res: &World) -> ClientManager {
        let mut message_channel = res.fetch_mut::<EventChannel<NetworkEvent>>();

        ClientManager {
            event_reader_id: message_channel.register_reader(),
        }
    }
}

impl<'a> System<'a> for ClientManager {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Read<'a, EventChannel<NetworkEvent>>,
        WriteStorage<'a, Synced>,
        WriteStorage<'a, SyncId>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Orientation>,
        WriteStorage<'a, Scale>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Acceleration>,
        WriteStorage<'a, Forces>,
        WriteStorage<'a, Mass>,
        WriteStorage<'a, CollisionObjectDescription>,
        WriteStorage<'a, Texture>,
        WriteStorage<'a, ContactMonitor>,
        Entities<'a>,
        Write<'a, SyncIdAllocator>,
        Write<'a, EventChannel<MessageToClient>>,
        Write<'a, Clients>,
        Write<'a, ServerStateUpdates>,
    );

    fn run(
        &mut self,
        (
            received_msg_chan,
            mut synced,
            mut sync_ids,
            mut positions,
            mut orientations,
            mut scales,
            mut velocities,
            mut accelerations,
            mut forces,
            mut masses,
            mut collision_objects,
            mut textures,
            mut contact_monitors,
            entities,
            mut sync_id_allocator,
            mut send_msg_chan,
            mut clients,
            mut state_updates,
        ): Self::SystemData,
    ) {
        let events = received_msg_chan.read(&mut self.event_reader_id);

        for event in events {
            match event {
                NetworkEvent::MessageFromClient(message) => {
                    if let Message::Spawn = message.message {
                        info!("Spawning for client {}", message.client_id);

                        let (spawned, sid) = self
                            .spawn(
                                &mut synced,
                                &mut sync_ids,
                                &mut positions,
                                &mut orientations,
                                &mut scales,
                                &mut velocities,
                                &mut accelerations,
                                &mut forces,
                                &mut masses,
                                &mut collision_objects,
                                &mut textures,
                                &mut contact_monitors,
                                &entities,
                                &mut sync_id_allocator,
                            )
                            .unwrap();

                        clients.client_entities.insert(message.client_id, spawned);

                        send_msg_chan.single_write(MessageToClient::new(
                            message.client_id,
                            Message::Spawned(msg::Spawned { sid }),
                        ));
                    }
                }
                NetworkEvent::ClientConnected(client_id) => {
                    info!("Registering new client {}", client_id);

                    let state_update = self.create_full_state_update(*client_id, &sync_ids);

                    clients.clients.push(*client_id);
                    state_updates
                        .client_updates
                        .insert(*client_id, state_update);
                }
                NetworkEvent::ClientDisconnected(client_id) => {
                    info!("Unregistering disconnected client {}", client_id);

                    let client_pos = clients.clients.iter().position(|c| c == client_id).unwrap();
                    clients.clients.swap_remove(client_pos);

                    if let Some(entity) = clients.client_entities.remove(client_id) {
                        debug!("Deleting entity for client {}", client_id);
                        entities.delete(entity).unwrap();
                    }

                    state_updates.client_updates.remove(client_id);
                }
            }
        }
    }
}

impl ClientManager {
    fn spawn(
        &self,
        synced: &mut WriteStorage<Synced>,
        sync_ids: &mut WriteStorage<SyncId>,
        positions: &mut WriteStorage<Position>,
        orientations: &mut WriteStorage<Orientation>,
        scales: &mut WriteStorage<Scale>,
        velocities: &mut WriteStorage<Velocity>,
        accelerations: &mut WriteStorage<Acceleration>,
        forces: &mut WriteStorage<Forces>,
        masses: &mut WriteStorage<Mass>,
        collision_objects: &mut WriteStorage<CollisionObjectDescription>,
        textures: &mut WriteStorage<Texture>,
        contact_monitors: &mut WriteStorage<ContactMonitor>,
        entities: &Entities,
        sync_id_allocator: &mut Write<SyncIdAllocator>,
    ) -> Result<(Entity, u32), specs::error::Error> {
        let entity = entities
            .build_entity()
            .with(Synced, synced)
            .with(Position(Vector2::new(0.0, 5.0)), positions)
            .with(Orientation(0.0), orientations)
            .with(Scale(Vector2::new(1.0, 1.0)), scales)
            .with(Velocity::default(), velocities)
            .with(Acceleration::default(), accelerations)
            .with(Forces::default(), forces)
            .with(Mass::new(70.0), masses)
            .with(
                CollisionObjectDescription {
                    behavior: CollisionObjectBehavior::Dynamic,
                    shape: ShapeHandle::new(shape::Cuboid::new(Vector2::new(0.2, 0.5))),
                },
                collision_objects,
            )
            .with(Texture::named("torch".to_string()), textures)
            .with(ContactMonitor::default(), contact_monitors)
            .build();

        let sync_id = sync_id_allocator.allocate(entity.id());
        let sid = sync_id.0;

        sync_ids.insert(entity, sync_id)?;

        Ok((entity, sid))
    }

    fn create_full_state_update(
        &self,
        _client_id: u32,
        sync_ids: &WriteStorage<SyncId>,
    ) -> ServerStateUpdate {
        let mut state_update = ServerStateUpdate::default();

        for id in sync_ids.join() {
            state_update.add_created_entity(id.0);
        }

        state_update
    }
}

/// Message sent from a specific client to the server.
#[derive(Debug)]
pub struct MessageFromClient {
    pub message: Message,
    pub client_id: u32,
}

impl MessageFromClient {
    pub fn new(client_id: u32, message: Message) -> MessageFromClient {
        MessageFromClient { client_id, message }
    }
}

#[derive(Debug)]
pub enum NetworkEvent {
    ClientConnected(u32),
    ClientDisconnected(u32),
    MessageFromClient(MessageFromClient),
}

/// Message that should be sent to a specific client from the server.
#[derive(Clone, Debug)]
pub struct MessageToClient {
    pub message: Message,
    pub client_id: u32,
}

impl MessageToClient {
    pub fn new(client_id: u32, message: Message) -> MessageToClient {
        MessageToClient { client_id, message }
    }
}

pub trait ComponentSerializer<C> {
    fn serialize(&self, into: &mut Vec<u8>);
}
