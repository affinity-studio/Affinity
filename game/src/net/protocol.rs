use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")]
pub enum Message {
    Connected(msg::Connected),
    Spawn,
    Spawned(msg::Spawned),
}

pub mod msg {
    use serde_derive::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct Connected {
        pub cid: u32,
    }

    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct Spawned {
        pub sid: u32,
    }
}
