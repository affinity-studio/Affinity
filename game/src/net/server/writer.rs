use crate::net::server::ServerStateUpdates;
use crate::net::SyncId;
use crate::net::Synced;
use crate::{
    ser::{BincodeComponentSerializer, ComponentSerializer, NullComponentSerializer},
    ComponentId,
};
use byteorder::{NetworkEndian, WriteBytesExt};
use log::trace;
use serde::Serialize;
use specs::Component;
use specs::DispatcherBuilder;
use specs::Join;
use specs::Read;
use specs::ReadExpect;
use specs::ReadStorage;
use specs::System;
use specs::Write;
use std::marker::PhantomData;

// #[derive(Default)]
pub struct Serialized<T> {
    _marker: PhantomData<T>,
    pub data: Vec<u8>,
}

impl<T> Default for Serialized<T> {
    fn default() -> Serialized<T> {
        Serialized {
            _marker: Default::default(),
            data: Vec::new(),
        }
    }
}

pub struct SerializedComponentCollector<T> {
    _marker: PhantomData<T>,
}

impl<T> Default for SerializedComponentCollector<T> {
    fn default() -> SerializedComponentCollector<T> {
        SerializedComponentCollector {
            _marker: Default::default(),
        }
    }
}

impl<'a, T> System<'a> for SerializedComponentCollector<T>
where
    T: Component,
    Serialized<T>: Default + Sync + Send,
    ComponentId<T>: Sync + Send,
{
    type SystemData = (
        Read<'a, Serialized<T>>,
        ReadExpect<'a, ComponentId<T>>,
        Write<'a, ServerStateUpdates>,
    );

    fn run(&mut self, (serialized, component_id, mut state_updates): Self::SystemData) {
        if !serialized.data.is_empty() {
            trace!(
                "Setting serialized component data for component {}",
                component_id.id
            );
            for update in state_updates.client_updates.values_mut() {
                update.set_component_states(component_id.id, &serialized.data);
            }
        }
    }
}

pub struct ServerComponentWriter<T, S> {
    _component_marker: PhantomData<T>,
    _serializer_marker: PhantomData<S>,
}

impl<T, S> Default for ServerComponentWriter<T, S> {
    fn default() -> ServerComponentWriter<T, S> {
        ServerComponentWriter {
            _component_marker: Default::default(),
            _serializer_marker: Default::default(),
        }
    }
}

impl<'a, T, S> System<'a> for ServerComponentWriter<T, S>
where
    T: Component,
    Serialized<T>: Default + Sync + Send,
    S: ComponentSerializer<T>,
{
    type SystemData = (
        ReadStorage<'a, T>,
        ReadStorage<'a, Synced>,
        ReadStorage<'a, SyncId>,
        Write<'a, Serialized<T>>,
    );

    fn run(&mut self, (components, synced, sync_ids, mut serialized): Self::SystemData) {
        // TODO: serialized data may be written multiple times because game may update faster than send.
        serialized.data.clear();

        for (component, _, sync_id) in (&components, &synced, &sync_ids).join() {
            serialized
                .data
                .write_u32::<NetworkEndian>(sync_id.0)
                .unwrap();
            S::serialize(&mut serialized.data, component);
        }
    }
}

pub trait WithServerComponentWriter {
    fn with_server_component_writer<T>(self, name: &str) -> Self
    where
        T: Component + Serialize + Send + Sync;

    fn with_server_null_component_writer<T>(self, name: &str) -> Self
    where
        T: Component + Send + Sync + Default;
}

impl<'a, 'b> WithServerComponentWriter for DispatcherBuilder<'a, 'b> {
    fn with_server_component_writer<T>(self, name: &str) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + Serialize + Send + Sync,
    {
        let serializer_name = format!("{}_serializer", name);
        self.with(
            ServerComponentWriter::<T, BincodeComponentSerializer<T>>::default(),
            &serializer_name,
            &[],
        )
        .with(
            SerializedComponentCollector::<T>::default(),
            &format!("serialized_{}_collector", name),
            &[&serializer_name],
        )
    }

    fn with_server_null_component_writer<T>(self, name: &str) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + Send + Sync + Default,
    {
        let serializer_name = format!("{}_serializer", name);
        self.with(
            ServerComponentWriter::<T, NullComponentSerializer<T>>::default(),
            &serializer_name,
            &[],
        )
        .with(
            SerializedComponentCollector::<T>::default(),
            &format!("serialized_{}_collector", name),
            &[&serializer_name],
        )
    }
}
