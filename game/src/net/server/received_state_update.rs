use crate::core::ecs::SystemName;
use affinity_derive::SystemName;
use hashbrown::HashMap;
use specs::{System, Write};

use crate::{net::component_update::ComponentStateUpdate, ComponentId};

#[derive(Default)]
pub struct ReceivedClientStateUpdates {
    pub client_updates: HashMap<u32, ReceivedClientStateUpdate>,
}

#[derive(Default)]
pub struct ReceivedClientStateUpdate {
    component_state: ComponentStateUpdate,
}

impl ReceivedClientStateUpdate {
    pub fn new(component_states_bytes: Vec<u8>) -> ReceivedClientStateUpdate {
        ReceivedClientStateUpdate {
            component_state: ComponentStateUpdate::new(component_states_bytes),
        }
    }

    pub fn clear(&mut self) {
        self.component_state.clear();
    }

    pub fn get_component_bytes<T>(&self, component_id: &ComponentId<T>) -> Option<&[u8]> {
        self.component_state.get_component_bytes(component_id)
    }
}

#[derive(SystemName)]
pub struct ReceviedClientStateUpdateResetter;

impl<'a> System<'a> for ReceviedClientStateUpdateResetter {
    type SystemData = Write<'a, ReceivedClientStateUpdates>;

    fn run(&mut self, mut state_updates: Self::SystemData) {
        state_updates.client_updates.clear();
    }
}
