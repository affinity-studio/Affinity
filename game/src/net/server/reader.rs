use crate::{
    ser::{BincodeComponentDeserializer, ComponentDeserializer, NullComponentDeserializer},
    ComponentId,
};
use log::trace;
use serde::de::DeserializeOwned;
use specs::DispatcherBuilder;
use specs::Read;
use specs::ReadExpect;
use specs::System;
use specs::WriteStorage;
use specs::{Component, Entity};
use std::io::Cursor;
use std::marker::PhantomData;

use super::{
    received_state_update::{ReceivedClientStateUpdate, ReceivedClientStateUpdates},
    Clients,
};

pub struct ServerComponentReader<T, S> {
    _marker: PhantomData<T>,
    _deserializer_marker: PhantomData<S>,
}

impl<T, S> Default for ServerComponentReader<T, S> {
    fn default() -> Self {
        ServerComponentReader {
            _marker: Default::default(),
            _deserializer_marker: Default::default(),
        }
    }
}

impl<'a, T, S> System<'a> for ServerComponentReader<T, S>
where
    T: Component + Send + Sync,
    ComponentId<T>: Send + Sync,
    S: ComponentDeserializer<T>,
{
    type SystemData = (
        WriteStorage<'a, T>,
        Read<'a, ReceivedClientStateUpdates>,
        ReadExpect<'a, ComponentId<T>>,
        Read<'a, Clients>,
    );

    fn run(&mut self, (mut components, state_updates, component_id, clients): Self::SystemData) {
        for (client_id, state_update) in &state_updates.client_updates {
            Self::update_client_entity_state(
                *client_id,
                state_update,
                &clients,
                &mut components,
                &component_id,
            );
        }
    }
}

impl<T, S> ServerComponentReader<T, S>
where
    T: Component + Send + Sync,
    ComponentId<T>: Send + Sync,
    S: ComponentDeserializer<T>,
{
    fn update_client_entity_state(
        client_id: u32,
        state_update: &ReceivedClientStateUpdate,
        clients: &Clients,
        components: &mut WriteStorage<T>,
        component_id: &ComponentId<T>,
    ) {
        if let Some(entity) = clients.client_entities.get(&client_id) {
            if let Some(bytes) = state_update.get_component_bytes(component_id) {
                trace!("Got component update: {}", bytes.len());
                Self::update_entity_state(entity, components, bytes);
            }
        }
    }

    fn update_entity_state(
        entity: &Entity,
        components: &mut WriteStorage<T>,
        component_bytes: &[u8],
    ) {
        let mut cursor = Cursor::new(component_bytes);
        while cursor.position() < component_bytes.len() as u64 {
            // Note: In order to move the cursor, the component must be deserialized even if entity does not exists
            let component = S::deserialize(&mut cursor);
            components.insert(*entity, component).unwrap();
        }
    }
}

pub trait WithServerComponentReader {
    fn with_server_component_reader<T>(self, name: &str) -> Self
    where
        T: Component + DeserializeOwned + Send + Sync;

    fn with_server_null_component_reader<T>(self, name: &str) -> Self
    where
        T: Component + Send + Sync + Default;
}

impl<'a, 'b> WithServerComponentReader for DispatcherBuilder<'a, 'b> {
    fn with_server_component_reader<T>(self, name: &str) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + DeserializeOwned + Send + Sync,
    {
        let deserializer_name = format!("{}_reader", name);
        self.with(
            ServerComponentReader::<T, BincodeComponentDeserializer<T>>::default(),
            &deserializer_name,
            &[],
        )
    }

    fn with_server_null_component_reader<T>(self, name: &str) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + Send + Sync + Default,
    {
        let deserializer_name = format!("{}_reader", name);
        self.with(
            ServerComponentReader::<T, NullComponentDeserializer<T>>::default(),
            &deserializer_name,
            &[],
        )
    }
}
