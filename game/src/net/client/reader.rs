use crate::net::client::EntitySyncIdMap;
use crate::net::client::ReceivedServerStateUpdates;
use crate::{
    ser::{BincodeComponentDeserializer, ComponentDeserializer, NullComponentDeserializer},
    ComponentId,
};
use byteorder::{NetworkEndian, ReadBytesExt};
use log::trace;
use serde::de::DeserializeOwned;
use specs::Component;
use specs::DispatcherBuilder;
use specs::Read;
use specs::ReadExpect;
use specs::System;
use specs::WriteStorage;
use std::io::Cursor;
use std::marker::PhantomData;

pub struct ClientComponentReader<T, S> {
    _marker: PhantomData<T>,
    _deserializer_marker: PhantomData<S>,
}

impl<T, S> Default for ClientComponentReader<T, S> {
    fn default() -> Self {
        ClientComponentReader {
            _marker: Default::default(),
            _deserializer_marker: Default::default(),
        }
    }
}

impl<'a, T, S> System<'a> for ClientComponentReader<T, S>
where
    T: Component + Send + Sync,
    ComponentId<T>: Send + Sync,
    S: ComponentDeserializer<T>,
{
    type SystemData = (
        WriteStorage<'a, T>,
        Read<'a, ReceivedServerStateUpdates>,
        ReadExpect<'a, ComponentId<T>>,
        Read<'a, EntitySyncIdMap>,
    );

    fn run(
        &mut self,
        (mut components, state_updates, component_id, sync_id_map): Self::SystemData,
    ) {
        for state_update in state_updates.current() {
            if let Some(bytes) = state_update.get_component_bytes(&component_id) {
                trace!("Got component update: {}", bytes.len());

                let mut cursor = Cursor::new(bytes);
                while cursor.position() < bytes.len() as u64 {
                    let sid = cursor.read_u32::<NetworkEndian>().unwrap();
                    trace!(
                        "Deserializing component id {} for sid {}",
                        component_id.id,
                        sid
                    );

                    // Note: In order to move the cursor, the component must be deserialized even if entity does not exists
                    let component = S::deserialize(&mut cursor);
                    if let Some(entity) = sync_id_map.entities.get(&sid) {
                        components.insert(*entity, component).unwrap();
                    }
                }
            }
        }
    }
}

pub trait WithClientComponentReader {
    fn with_client_component_reader<T>(self, name: &str) -> Self
    where
        T: Component + DeserializeOwned + Send + Sync;

    fn with_client_null_component_reader<T>(self, name: &str) -> Self
    where
        T: Component + Send + Sync + Default;
}

impl<'a, 'b> WithClientComponentReader for DispatcherBuilder<'a, 'b> {
    fn with_client_component_reader<T>(self, name: &str) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + DeserializeOwned + Send + Sync,
    {
        let deserializer_name = format!("{}_reader", name);
        self.with(
            ClientComponentReader::<T, BincodeComponentDeserializer<T>>::default(),
            &deserializer_name,
            &[],
        )
    }

    fn with_client_null_component_reader<T>(self, name: &str) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + Send + Sync + Default,
    {
        let deserializer_name = format!("{}_reader", name);
        self.with(
            ClientComponentReader::<T, NullComponentDeserializer<T>>::default(),
            &deserializer_name,
            &[],
        )
    }
}
