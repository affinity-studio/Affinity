use crate::{
    net::Synced,
    ser::{BincodeComponentSerializer, ComponentSerializer, NullComponentSerializer},
    ComponentId,
};
use byteorder::{ByteOrder, NetworkEndian};
use serde::Serialize;
use specs::{Component, DispatcherBuilder, Join, ReadExpect, ReadStorage, System, Write};
use std::marker::PhantomData;

use super::state_update::ClientStateUpdate;

pub struct ClientComponentWriter<T, S> {
    _component_marker: PhantomData<T>,
    _serializer_marker: PhantomData<S>,
}

impl<T, S> Default for ClientComponentWriter<T, S> {
    fn default() -> ClientComponentWriter<T, S> {
        Self {
            _component_marker: Default::default(),
            _serializer_marker: Default::default(),
        }
    }
}

impl<'a, T, S> System<'a> for ClientComponentWriter<T, S>
where
    T: Component,
    S: ComponentSerializer<T>,
    ComponentId<T>: Sync + Send,
{
    type SystemData = (
        ReadStorage<'a, T>,
        ReadStorage<'a, Synced>,
        ReadExpect<'a, ComponentId<T>>,
        Write<'a, ClientStateUpdate>,
    );

    fn run(&mut self, (components, synced, component_id, mut state_update): Self::SystemData) {
        let bytes = &mut state_update.component_states_bytes;

        // 8-bit component ID
        bytes.push(component_id.id);

        // 16-bit temporary length of component bytes
        let length_index = bytes.len();
        bytes.extend_from_slice(&[0, 0]);

        // Component bytes
        for (component, _) in (&components, &synced).join() {
            S::serialize(bytes, component);
        }

        // Update length bytes with actual length
        let component_bytes_start_index = length_index + 2;
        let length = (bytes.len() - component_bytes_start_index) as u16;
        NetworkEndian::write_u16(&mut bytes[length_index..length_index + 2], length);
    }
}

pub trait WithClientComponentWriter {
    fn with_client_component_writer<T>(self, name: &str, dep: &[&str]) -> Self
    where
        T: Component + Serialize + Send + Sync;

    fn with_client_null_component_writer<T>(self, name: &str, dep: &[&str]) -> Self
    where
        T: Component + Send + Sync + Default;
}

impl<'a, 'b> WithClientComponentWriter for DispatcherBuilder<'a, 'b> {
    fn with_client_component_writer<T>(self, name: &str, dep: &[&str]) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + Serialize + Send + Sync,
    {
        let serializer_name = format!("{}_serializer", name);
        self.with(
            ClientComponentWriter::<T, BincodeComponentSerializer<T>>::default(),
            &serializer_name,
            dep,
        )
    }

    fn with_client_null_component_writer<T>(
        self,
        name: &str,
        dep: &[&str],
    ) -> DispatcherBuilder<'a, 'b>
    where
        T: Component + Send + Sync + Default,
    {
        let serializer_name = format!("{}_serializer", name);
        self.with(
            ClientComponentWriter::<T, NullComponentSerializer<T>>::default(),
            &serializer_name,
            dep,
        )
    }
}
