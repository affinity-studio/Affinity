#[derive(Default)]
pub struct ClientStateUpdate {
    pub component_states_bytes: Vec<u8>,
}

impl ClientStateUpdate {
    pub fn clear(&mut self) {
        self.component_states_bytes.clear();
    }
}
