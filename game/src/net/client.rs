use self::state_update::ClientStateUpdate;

use super::component_update::ComponentStateUpdate;
use super::{Message, SyncId, Synced};
use crate::control::Control;
use crate::core::ecs::SystemName;
use crate::{camera::ActiveCamera, components::Target, ComponentId};
use affinity_derive::SystemName;
use byteorder::{ByteOrder, NetworkEndian};
use hashbrown::HashMap;
use log::{debug, trace};
use shrev::EventChannel;
use specs::prelude::*;

pub mod reader;
pub mod state_update;
pub mod writer;

#[derive(Default)]
pub struct ReceivedServerStateUpdate {
    deleted_entities: Vec<u32>,
    created_entities: Vec<u32>,
    component_state: ComponentStateUpdate,
}

impl ReceivedServerStateUpdate {
    pub fn new() -> ReceivedServerStateUpdate {
        Default::default()
    }

    pub fn clear_and_read(&mut self, bytes: &[u8]) {
        trace!("update: {:?}", bytes);
        let deleted_entities_start = 2;
        let deleted_entities_len =
            NetworkEndian::read_u16(&bytes[0..deleted_entities_start]) as usize;
        let deleted_entities_end = deleted_entities_start + deleted_entities_len;
        let deleted_entities_bytes = &bytes[deleted_entities_start..deleted_entities_end];

        self.deleted_entities.clear();
        self.deleted_entities.extend(
            deleted_entities_bytes
                .chunks(4)
                .map(NetworkEndian::read_u32),
        );

        let created_entities_start = deleted_entities_end + 2;
        let created_entities_len =
            NetworkEndian::read_u16(&bytes[deleted_entities_end..created_entities_start]) as usize;
        let created_entities_end = created_entities_start + created_entities_len;
        let created_entities_bytes = &bytes[created_entities_start..created_entities_end];

        self.created_entities.clear();
        self.created_entities.extend(
            created_entities_bytes
                .chunks(4)
                .map(NetworkEndian::read_u32),
        );

        let component_states_start = created_entities_end + 2;
        let component_states_len =
            NetworkEndian::read_u16(&bytes[created_entities_end..component_states_start]);
        let component_states_end = component_states_start + component_states_len as usize;

        self.component_state
            .update(&bytes[component_states_start..component_states_end]);
    }

    pub fn clear(&mut self) {
        self.deleted_entities.clear();
        self.created_entities.clear();
        self.component_state.clear();
    }

    pub fn get_component_bytes<T>(&self, component_id: &ComponentId<T>) -> Option<&[u8]> {
        self.component_state.get_component_bytes(component_id)
    }
}

#[derive(Default)]
pub struct ReceivedServerStateUpdates {
    updates: Vec<ReceivedServerStateUpdate>,
    free_start: usize,
}

impl ReceivedServerStateUpdates {
    pub fn current(&self) -> &[ReceivedServerStateUpdate] {
        &self.updates[0..self.free_start]
    }

    pub fn add(&mut self, bytes: &[u8]) {
        let update = if self.free_start < self.updates.len() {
            // Reuse freed update
            &mut self.updates[self.free_start]
        } else {
            // Create new update
            self.updates.push(ReceivedServerStateUpdate::default());
            self.updates.last_mut().unwrap()
        };

        update.clear_and_read(bytes);

        self.free_start += 1;
    }

    #[allow(dead_code)]
    fn num_current(&self) -> usize {
        self.free_start
    }

    #[allow(dead_code)]
    fn num_free(&self) -> usize {
        self.updates.len() - self.free_start
    }

    fn free_current(&mut self) {
        self.free_start = 0;
    }
}

#[derive(Default)]
pub struct EntitySyncIdMap {
    entities: HashMap<u32, Entity>,
}

/// System that manages the core synchronization of Entities with the Synced component.
pub struct Synchronizer;

impl<'a> System<'a> for Synchronizer {
    type SystemData = (
        WriteStorage<'a, Synced>,
        WriteStorage<'a, SyncId>,
        Entities<'a>,
        Read<'a, ReceivedServerStateUpdates>,
        Write<'a, EntitySyncIdMap>,
    );

    fn run(
        &mut self,
        (mut synced, mut sync_ids, entities, state_updates, mut sync_id_map): Self::SystemData,
    ) {
        for state_update in state_updates.current() {
            if !state_update.deleted_entities.is_empty() {
                debug!("Deleted entities: {:?}", state_update.deleted_entities);
            }
            if !state_update.created_entities.is_empty() {
                debug!("Created entities: {:?}", state_update.created_entities);
            }
            // if !state_update.component_states_bytes.is_empty() {
            //     debug!(
            //         "Component states: {:?}",
            //         state_update.component_states_bytes
            //     );
            // }

            for sid in &state_update.deleted_entities {
                debug!("Deleting entity with SID {}", sid);

                if let Some(entity) = sync_id_map.entities.remove(sid) {
                    entities.delete(entity).unwrap();
                }
            }

            for sid in &state_update.created_entities {
                debug!("Creating entity with SID {}", sid);

                let sync_id = SyncId(*sid);
                let entity = entities
                    .build_entity()
                    .with(Synced, &mut synced)
                    .with(sync_id, &mut sync_ids)
                    .build();

                sync_id_map.entities.insert(*sid, entity);
            }

            // TODO: Delete removed Compoentns.
            // TODO: Create missing Components.
            // TODO: Update existing Components.

            // TODO: How to handle Entities removed on client? Re-create them? Prevent removal?
            // TODO: Consider changing Component state updates, as repeatedly syncing fairly static components wastes bandwidth.
            // TODO: How to handle temporary Components, such as LoadingTexture?
        }
    }
}

impl SystemName for Synchronizer {
    const NAME: &'static str = "Synchronizer";
}

#[derive(SystemName)]
pub struct ClientStateUpdateResetter;

impl<'a> System<'a> for ClientStateUpdateResetter {
    type SystemData = Write<'a, ClientStateUpdate>;

    fn run(&mut self, mut state_update: Self::SystemData) {
        state_update.clear();
    }
}

#[derive(SystemName)]
pub struct ReceviedServerStateUpdateResetter;

impl<'a> System<'a> for ReceviedServerStateUpdateResetter {
    type SystemData = Write<'a, ReceivedServerStateUpdates>;

    fn run(&mut self, mut state_updates: Self::SystemData) {
        state_updates.free_current();
    }
}

enum PlayerEntityStatus {
    Pending,
    Spawned(u32),
    Controlled(Entity),
}

/// System to ensure that spawned player entity is controlled
#[derive(SystemName)]
pub struct PlayerEntityController {
    status: PlayerEntityStatus,
    message_reader: ReaderId<Message>,
}

impl PlayerEntityController {
    pub fn new(world: &World) -> PlayerEntityController {
        let mut message_channel = world.fetch_mut::<EventChannel<Message>>();

        PlayerEntityController {
            status: PlayerEntityStatus::Pending,
            message_reader: message_channel.register_reader(),
        }
    }

    fn take_control<'a>(
        &self,
        entity: Entity,
        active_camera: &ActiveCamera,
        targets: &mut WriteStorage<'a, Target>,
        controls: &mut WriteStorage<'a, Control>,
    ) {
        let camera_entity = active_camera.0;
        targets.insert(camera_entity, Target(entity)).unwrap();

        // TODO: Should the server add this component without updating the state?
        controls.insert(entity, Control::default()).unwrap();
    }
}

impl<'a> System<'a> for PlayerEntityController {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Read<'a, EntitySyncIdMap>,
        Read<'a, EventChannel<Message>>,
        ReadExpect<'a, ActiveCamera>,
        WriteStorage<'a, Target>,
        WriteStorage<'a, Control>,
    );

    fn run(
        &mut self,
        (entity_sync_ids, messages, active_camera, mut targets, mut controls): Self::SystemData,
    ) {
        for message in messages.read(&mut self.message_reader) {
            if let Message::Spawned(data) = message {
                self.status = PlayerEntityStatus::Spawned(data.sid);
            }
        }

        if let PlayerEntityStatus::Spawned(sid) = self.status {
            if let Some(entity) = entity_sync_ids.entities.get(&sid) {
                self.take_control(*entity, &active_camera, &mut targets, &mut controls);
                self.status = PlayerEntityStatus::Controlled(*entity);
            }
        }
    }
}
