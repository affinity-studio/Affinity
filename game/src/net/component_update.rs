use std::io::{Cursor, Seek, SeekFrom};

use byteorder::{NetworkEndian, ReadBytesExt};
use hashbrown::HashMap;

use crate::ComponentId;

type ComponentLocations = HashMap<u8, (usize, usize)>;

#[derive(Default)]
pub struct ComponentStateUpdate {
    component_states_bytes: Vec<u8>,
    component_locations: ComponentLocations,
}

impl ComponentStateUpdate {
    pub fn new(component_states_bytes: Vec<u8>) -> ComponentStateUpdate {
        let component_locations = Self::read_component_locations(&component_states_bytes);
        ComponentStateUpdate {
            component_states_bytes,
            component_locations,
        }
    }

    pub fn clear(&mut self) {
        self.component_states_bytes.clear();
        self.component_locations.clear();
    }

    pub fn update(&mut self, component_states_bytes: &[u8]) {
        self.component_states_bytes.clear();
        self.component_states_bytes
            .extend_from_slice(component_states_bytes);
        self.component_locations = Self::read_component_locations(component_states_bytes);
    }

    pub fn get_component_bytes<T>(&self, component_id: &ComponentId<T>) -> Option<&[u8]> {
        let id = component_id.id;

        self.component_locations.get(&id).map(|(start, len)| {
            let end = start + len;
            &self.component_states_bytes[*start..end]
        })
    }

    fn read_component_locations(update_bytes: &[u8]) -> ComponentLocations {
        let component_states_len = update_bytes.len() as u64;
        let mut component_reader = Cursor::new(&update_bytes);
        let mut component_locations = ComponentLocations::default();

        while component_reader.position() < component_states_len {
            let component_id = component_reader.read_u8().unwrap();
            let components_len = component_reader.read_u16::<NetworkEndian>().unwrap() as usize;
            let start_pos = component_reader.position() as usize;

            component_locations.insert(component_id, (start_pos, components_len));

            component_reader
                .seek(SeekFrom::Current(components_len as i64))
                .unwrap();
        }

        component_locations
    }
}
