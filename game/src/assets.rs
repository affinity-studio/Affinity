use hashbrown::HashMap;
use std::{
    fmt::{self, Display, Formatter},
    hash::{Hash, Hasher},
    sync::{Arc, RwLock},
};

#[derive(Debug)]
pub struct Error {
    inner: Option<Box<dyn std::error::Error>>,
    kind: ErrorKind,
}

#[derive(Debug)]
pub enum ErrorKind {
    NotFound,
    Other(String),
}

pub struct Assets {
    assets: Vec<Asset>,
    asset_map: HashMap<String, Handle>,
}

pub struct Asset {
    pub name: String,
    pub data: Vec<u8>,
}

#[derive(Clone)]
pub struct Handle {
    state: Arc<RwLock<State>>,
    name: String,
}

enum State {
    Queued(Option<Box<dyn FnMut(&Asset) + Send + Sync>>),
    Loading(Option<Box<dyn FnMut(&Asset) + Send + Sync>>),
    Loaded(usize),
}

impl Handle {
    pub fn name(&self) -> &str {
        &self.name
    }

    fn mark_as_loading(&mut self) -> Result<(), ()> {
        let mut state = self.state.write().map_err(|_| ())?;
        match *state {
            State::Queued(ref mut callback) => {
                let callback = callback.take();
                *state = State::Loading(callback);

                Ok(())
            }
            _ => Err(()),
        }
    }

    fn queued_with_callback<F: Into<Box<dyn FnMut(&Asset) + Send + Sync>>>(
        name: String,
        callback: F,
    ) -> Handle {
        Handle {
            state: Arc::new(RwLock::new(State::Queued(Some(callback.into())))),
            name,
        }
    }

    fn queued(name: String) -> Handle {
        Handle {
            state: Arc::new(RwLock::new(State::Queued(None))),
            name,
        }
    }

    fn is_queued(&self) -> bool {
        self.state.read().unwrap().is_queued()
    }
}

impl PartialEq for Handle {
    fn eq(&self, other: &Handle) -> bool {
        self.name == other.name
    }
}

impl Eq for Handle {}

impl Hash for Handle {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state)
    }
}

impl State {
    fn is_queued(&self) -> bool {
        matches!(self, State::Queued(_))
    }
}

// TODO: This isn't exactly correct for States that ar not Loaded.
// impl PartialEq for State {
//     fn eq(&self, other: &State) -> bool {
//         match self {
//             State::Loaded(index) => match other {
//                 State::Loaded(other_index) => index == other_index,
//                 _ => false,
//             },
//             _ => false,
//         }
//     }
// }

// impl Eq for State {}

impl Hash for State {
    fn hash<H: Hasher>(&self, state: &mut H) {
        if let State::Loaded(index) = self {
            index.hash(state)
        }
    }
}

impl Assets {
    pub fn new() -> Assets {
        Assets {
            assets: vec![],
            asset_map: HashMap::default(),
        }
    }

    pub fn load(&mut self, name: &str) -> Handle {
        self.asset_map
            .entry(name.into())
            .or_insert_with(|| Handle::queued(name.into()))
            .clone()
    }

    pub fn load_with_callback<F>(&mut self, name: &str, callback: F) -> Handle
    where
        F: Into<Box<dyn FnMut(&Asset) + Send + Sync>>,
    {
        self.asset_map
            .entry(name.into())
            .or_insert_with(|| Handle::queued_with_callback(name.into(), callback))
            .clone()
    }

    pub fn take_queued_load(&mut self) -> Option<&str> {
        if let Some((name, handle)) = self
            .asset_map
            .iter_mut()
            .find(|(_, handle)| handle.is_queued())
        {
            handle.mark_as_loading().unwrap();
            Some(name)
        } else {
            None
        }
    }

    pub fn finish_load(&mut self, asset: Asset) {
        if let Some(handle) = self.asset_map.get(&asset.name) {
            let mut state = handle.state.write().unwrap();
            match *state {
                State::Loading(ref mut callback) => {
                    let index = self.assets.len();
                    self.assets.push(asset);

                    if let Some(callback) = callback {
                        (*callback)(self.assets.last().unwrap());
                    }

                    *state = State::Loaded(index);
                }
                State::Loaded(_) => panic!("Asset already loaded"),
                State::Queued(_) => panic!("Asset only queued"),
            }
        }
    }

    pub fn loaded(&self, name: &str) -> bool {
        if let Some(handle) = self.asset_map.get(name) {
            matches!(*handle.state.read().unwrap(), State::Loaded(_))
        } else {
            false
        }
    }

    pub fn loading(&self, name: &str) -> bool {
        if let Some(handle) = self.asset_map.get(name) {
            matches!(
                *handle.state.read().unwrap(),
                State::Queued(_) | State::Loading(_)
            )
        } else {
            false
        }
    }

    pub fn needs_loading(&self, name: &str) -> bool {
        !self.loaded(name) && !self.loading(name)
    }

    pub fn get_handle(&self, name: &str) -> Option<Handle> {
        self.asset_map.get(name).cloned()
    }

    pub fn get(&self, handle: &Handle) -> Option<&Asset> {
        if let State::Loaded(index) = *handle.state.read().unwrap() {
            Some(&self.assets[index])
        } else {
            None
        }
    }
}

impl Default for Assets {
    fn default() -> Assets {
        Assets::new()
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        match self.kind {
            ErrorKind::NotFound => write!(f, "Asset could not be founf"),
            ErrorKind::Other(ref description) => write!(f, "Other asset error: {}", description),
        }
    }
}

impl std::error::Error for Error {
    fn cause(&self) -> Option<&dyn std::error::Error> {
        self.inner.as_ref().map(AsRef::as_ref)
    }
}

impl Error {
    pub fn new(kind: ErrorKind) -> Error {
        Error { kind, inner: None }
    }

    pub fn from_inner<E>(kind: ErrorKind, inner: E) -> Error
    where
        E: Into<Box<dyn std::error::Error + Send + Sync>>,
    {
        Error {
            kind,
            inner: Some(inner.into()),
        }
    }
}
