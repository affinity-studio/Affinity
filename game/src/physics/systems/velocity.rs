use specs::prelude::*;

use crate::{
    components::{Orientation, Position},
    core::ecs::SystemName,
    physics::components::{AngularVelocity, Velocity},
    resources::DeltaTime,
};

pub struct VelocityApplier;

impl<'a> System<'a> for VelocityApplier {
    type SystemData = (
        WriteStorage<'a, Position>,
        ReadStorage<'a, Velocity>,
        Read<'a, DeltaTime>,
    );

    fn run(&mut self, (mut positions, velocities, deltatime): Self::SystemData) {
        if let Some(multiplier) = deltatime.fixed_multiplier() {
            for (position, velocity) in (&mut positions, &velocities).join() {
                position.0 += velocity.0 * multiplier;
            }
        }
    }
}

impl SystemName for VelocityApplier {
    const NAME: &'static str = "velocity_applier";
}

pub struct AngularVelocityApplier;

impl<'a> System<'a> for AngularVelocityApplier {
    type SystemData = (
        WriteStorage<'a, Orientation>,
        ReadStorage<'a, AngularVelocity>,
        Read<'a, DeltaTime>,
    );

    fn run(&mut self, (mut orientations, velocities, deltatime): Self::SystemData) {
        for (orientation, velocity) in (&mut orientations, &velocities).join() {
            orientation.0 += velocity.0 * deltatime.multiplier();
        }
    }
}

impl SystemName for AngularVelocityApplier {
    const NAME: &'static str = "angular_velocity_applier";
}
