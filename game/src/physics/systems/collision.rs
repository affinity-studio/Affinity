use crate::resources::DeltaTime;
use log::{debug, trace};
use nalgebra::{self as na, Isometry2, Vector2};
use ncollide2d::{
    pipeline::{
        CollisionGroups, CollisionObjectSlabHandle, ContactEvent, GeometricQueryType,
        ProximityEvent,
    },
    query::ContactManifold,
    world::CollisionWorld,
};
use shrev::EventChannel;
use specs::{prelude::*, Join};

use crate::{
    components::Position,
    core::ecs::SystemName,
    physics::{
        components::{CollisionObject, CollisionObjectDescription, ContactMonitor, Mass, Velocity},
        BeginContact, Contact, ContactEvent as PhysicsContactEvent, EndContact,
    },
};

pub struct Collision {
    world: CollisionWorld<f32, BodyData>,
}

impl<'a> System<'a> for Collision {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, CollisionObject>,
        WriteStorage<'a, CollisionObjectDescription>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, ContactMonitor>,
        ReadStorage<'a, Mass>,
        Write<'a, EventChannel<PhysicsContactEvent>>,
        Read<'a, DeltaTime>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut collision_objects,
            mut collision_object_descriptions,
            mut positions,
            mut velocities,
            mut contact_monitors,
            masses,
            mut contact_events,
            delta_time,
        ): Self::SystemData,
    ) {
        self.create_collision_objects(
            &entities,
            &mut collision_object_descriptions,
            &mut collision_objects,
            &positions,
        );

        if delta_time.fixed.is_some() {
            for _ in 0..5 {
                for (collision_object, position) in (&collision_objects, &positions).join() {
                    let object = self.world.get_mut(collision_object.handle).unwrap();
                    object.set_position(Isometry2::new(position.0, na::zero()));
                }

                self.world.update();

                // for event in self.world.proximity_events() {
                //     self.handle_proximity_event(event)
                // }

                // for event in self.world.contact_events() {
                //     self.handle_contact_event(event, &mut contact_events)
                // }

                for (handle1, handle2, _alg, manifold) in self.world.contact_pairs(true) {
                    let co1 = self.world.collision_object(handle1).unwrap();
                    let co2 = self.world.collision_object(handle2).unwrap();

                    self.resolve_collision(
                        co1,
                        co2,
                        manifold,
                        &masses,
                        &mut positions,
                        &mut velocities,
                    );
                }
            }

            self.update_contact_monitors(&mut contact_monitors);
        }
    }
}

impl Collision {
    fn handle_proximity_event(&self, _event: &ProximityEvent<CollisionObjectSlabHandle>) {
        // debug!("Proximity event: {:?}", event);
    }

    fn handle_contact_event(
        &self,
        event: &ContactEvent<CollisionObjectSlabHandle>,
        contact_events: &mut EventChannel<PhysicsContactEvent>,
    ) {
        // debug!("Contact event: {:?}", event);
        match *event {
            ContactEvent::Started(handle1, handle2) => {
                // FIXME: Contact pair may not exist even though contact has started. ignore?
                let (_, _, _alg, manifold) =
                    self.world.contact_pair(handle1, handle2, true).unwrap();

                let co1 = self.world.collision_object(handle1).unwrap();
                let co2 = self.world.collision_object(handle2).unwrap();

                contact_events.single_write(PhysicsContactEvent::BeginContact(BeginContact {
                    a: co1.data().entity,
                    b: co2.data().entity,
                    contacts: manifold
                        .contacts()
                        .map(|c| Contact {
                            world1: c.contact.world1,
                            world2: c.contact.world2,
                            normal: c.contact.normal,
                        })
                        .collect(),
                }));
            }
            ContactEvent::Stopped(handle1, handle2) => {
                let co1 = self.world.collision_object(handle1).unwrap();
                let co2 = self.world.collision_object(handle2).unwrap();

                contact_events.single_write(PhysicsContactEvent::EndContact(EndContact {
                    a: co1.data().entity,
                    b: co2.data().entity,
                }));
            }
        }
    }

    fn create_collision_objects(
        &mut self,
        entities: &Entities,
        collision_object_descriptions: &mut WriteStorage<CollisionObjectDescription>,
        collision_objects: &mut WriteStorage<CollisionObject>,
        positions: &WriteStorage<Position>,
    ) {
        for (entity, collision_object_description, position) in
            (entities, collision_object_descriptions.drain(), positions).join()
        {
            debug!("Creating new collision object at {}", position.0);

            let collision_groups = CollisionGroups::new();
            let (collision_object_handle, _) = self.world.add(
                Isometry2::new(position.0, na::zero()),
                collision_object_description.shape,
                collision_groups,
                GeometricQueryType::Contacts(0.0, 0.0),
                BodyData { entity },
            );

            let collision_object = CollisionObject {
                handle: collision_object_handle,
            };
            collision_objects.insert(entity, collision_object).unwrap();
        }
    }

    fn resolve_collision(
        &self,
        co1: &ncollide2d::pipeline::CollisionObject<f32, BodyData>,
        co2: &ncollide2d::pipeline::CollisionObject<f32, BodyData>,
        manifold: &ContactManifold<f32>,
        masses: &ReadStorage<Mass>,
        positions: &mut WriteStorage<Position>,
        velocities: &mut WriteStorage<Velocity>,
    ) {
        let entity1 = co1.data().entity;
        let entity2 = co2.data().entity;

        if let Some((entity1_mass, entity2_mass)) = get_collision_masses(entity1, entity2, masses) {
            let deepest_contact = manifold.deepest_contact().unwrap();
            let contact_normal = deepest_contact.contact.normal.into_inner();
            let contact_depth = deepest_contact.contact.depth;

            let entity1_velocity = velocities.get(entity1).map(|v| v.0);
            let entity2_velocity = velocities.get(entity2).map(|v| v.0);

            if let Some(entity1_velocity) = entity1_velocity {
                self.resolve_entity_collision(
                    entity1,
                    &entity1_velocity,
                    entity1_mass.inv_mass,
                    &entity2_velocity.unwrap_or_else(|| Vector2::new(0.0, 0.0)),
                    entity2_mass.inv_mass,
                    &-contact_normal,
                    contact_depth,
                    positions,
                    velocities,
                );
            }

            if let Some(entity2_velocity) = entity2_velocity {
                self.resolve_entity_collision(
                    entity2,
                    &entity2_velocity,
                    entity2_mass.inv_mass,
                    &entity1_velocity.unwrap_or_else(|| Vector2::new(0.0, 0.0)),
                    entity1_mass.inv_mass,
                    &contact_normal,
                    contact_depth,
                    positions,
                    velocities,
                );
            }
        }
    }

    fn resolve_entity_collision(
        &self,
        entity: Entity,
        velocity: &Vector2<f32>,
        inv_mass: f32,
        other_velocity: &Vector2<f32>,
        other_inv_mass: f32,
        contact_normal: &Vector2<f32>,
        contact_depth: f32,
        positions: &mut WriteStorage<Position>,
        velocities: &mut WriteStorage<Velocity>,
    ) {
        let relative_vel = velocity - other_velocity;
        let contact_speed = relative_vel.dot(contact_normal);

        if contact_speed <= 0.0 {
            const RESTITUTION: f32 = 0.0;
            let impulse_scalar =
                (-(1.0 + RESTITUTION) * contact_speed) / (inv_mass + other_inv_mass);
            let impulse = contact_normal * impulse_scalar;

            let velocity_comp = velocities.get_mut(entity).unwrap();
            velocity_comp.0 += inv_mass * impulse;
            trace!("applied impulse {}", impulse);
        }

        const PENETRATION_ALLOWANCE: f32 = 0.01;
        const PENETRATION_PERCENT: f32 = 0.5;
        let penetration_correction = ((contact_depth - PENETRATION_ALLOWANCE).max(0.0)
            / (inv_mass + other_inv_mass))
            * PENETRATION_PERCENT
            * contact_normal;

        let position_comp = positions.get_mut(entity).unwrap();
        position_comp.0 += penetration_correction * inv_mass;
        trace!("applied penetration correction {}", penetration_correction);
    }

    fn update_contact_monitors(&mut self, contact_monitors: &mut WriteStorage<ContactMonitor>) {
        for monitor in contact_monitors.join() {
            monitor.contacts.clear();
        }

        for (handle1, handle2, _alg, manifold) in self.world.contact_pairs(true) {
            let co1 = self.world.collision_object(handle1).unwrap();
            let co2 = self.world.collision_object(handle2).unwrap();

            if let Some(monitor) = contact_monitors.get_mut(co1.data().entity) {
                for contact in manifold.contacts() {
                    monitor.contacts.push(Contact {
                        world1: contact.contact.world1,
                        world2: contact.contact.world2,
                        normal: -contact.contact.normal,
                    });
                }
            }

            if let Some(monitor) = contact_monitors.get_mut(co2.data().entity) {
                for contact in manifold.contacts() {
                    monitor.contacts.push(Contact {
                        world1: contact.contact.world1,
                        world2: contact.contact.world2,
                        normal: contact.contact.normal,
                    });
                }
            }
        }
    }
}

fn get_collision_masses<'a>(
    entity1: Entity,
    entity2: Entity,
    masses: &'a ReadStorage<Mass>,
) -> Option<(&'a Mass, &'a Mass)> {
    if let Some(entity1_mass) = masses.get(entity1) {
        if let Some(entity2_mass) = masses.get(entity2) {
            return Some((entity1_mass, entity2_mass));
        }
    }

    None
}

impl SystemName for Collision {
    const NAME: &'static str = "Collision";
}

impl Default for Collision {
    fn default() -> Self {
        Collision {
            world: CollisionWorld::new(0.02),
        }
    }
}

struct BodyData {
    entity: Entity,
}
