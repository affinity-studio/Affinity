use specs::prelude::*;

use crate::{
    core::ecs::SystemName,
    physics::components::{Acceleration, Forces, Mass},
};

pub struct ForceApplier;

impl<'a> System<'a> for ForceApplier {
    type SystemData = (
        WriteStorage<'a, Acceleration>,
        WriteStorage<'a, Forces>,
        ReadStorage<'a, Mass>,
    );

    fn run(&mut self, (mut accelerations, mut forces_components, masses): Self::SystemData) {
        for (acceleration, forces, mass) in
            (&mut accelerations, &mut forces_components, &masses).join()
        {
            for force in &forces.0 {
                acceleration.0 += *force * mass.inv_mass;
            }

            forces.0.clear();
        }
    }
}

impl SystemName for ForceApplier {
    const NAME: &'static str = "ForceApplier";
}
