use nalgebra::Vector2;
use specs::prelude::*;

use crate::{
    core::ecs::SystemName,
    physics::components::{Forces, Mass},
};

pub struct UniversalGravitation;

impl<'a> System<'a> for UniversalGravitation {
    type SystemData = (WriteStorage<'a, Forces>, ReadStorage<'a, Mass>);

    fn run(&mut self, (mut forces_components, masses): Self::SystemData) {
        for (forces, mass) in (&mut forces_components, &masses).join() {
            const G: f32 = 6.67e-11;
            const PLANET_MASS: f32 = 5.9722e24;
            const PLANET_RADIUS: f32 = 6362.0e3;
            // TODO: Create Planet component with mass and radius?

            let force = (G * PLANET_MASS) / (PLANET_RADIUS * PLANET_RADIUS * mass.inv_mass);

            forces.0.push(Vector2::new(0.0, -force));
        }
    }
}

impl SystemName for UniversalGravitation {
    const NAME: &'static str = "UniversalGravitation";
}
