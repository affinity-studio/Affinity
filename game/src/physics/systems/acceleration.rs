use specs::prelude::*;
use nalgebra::Vector2;

use crate::{
    resources::DeltaTime,
    core::ecs::SystemName,
    physics::components::{Velocity, Acceleration},
};

pub struct Accelerator;

impl<'a> System<'a> for Accelerator {
    type SystemData = (
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Acceleration>,
        Read<'a, DeltaTime>,
    );

    fn run(&mut self, (mut velocities, mut accelerations, deltatime): Self::SystemData) {
        if let Some(multiplier) = deltatime.fixed_multiplier() {
            for (velocity, acceleration) in (&mut velocities, &mut accelerations).join() {
                velocity.0 += acceleration.0 * multiplier;
                acceleration.0 = Vector2::new(0.0, 0.0);
            }
        }
    }
}

impl SystemName for Accelerator {
    const NAME: &'static str = "accelerator";
}