use nalgebra::Vector2;
use ncollide2d::{shape::ShapeHandle, pipeline::CollisionObjectSlabHandle};
use serde_derive::{Deserialize, Serialize};
use specs::{Component, VecStorage};
use specs_derive::Component;

use super::{CollisionObjectBehavior, Contact};

#[derive(Component, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Velocity(
    #[serde(serialize_with = "crate::core::ser::serialize_vec2")]
    #[serde(deserialize_with = "crate::core::ser::deserialize_vec2")]
    pub Vector2<f32>,
);

impl Default for Velocity {
    fn default() -> Velocity {
        Velocity(Vector2::new(0.0, 0.0))
    }
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct AngularVelocity(pub f32);

impl Default for AngularVelocity {
    fn default() -> AngularVelocity {
        AngularVelocity(0.0)
    }
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Mass {
    pub inv_mass: f32,
}

impl Mass {
    pub fn new(mass: f32) -> Mass {
        Mass {
            inv_mass: 1.0 / mass,
        }
    }

    pub fn mass(&self) -> f32 {
        1.0 / self.inv_mass
    }
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Acceleration(pub Vector2<f32>);

impl Default for Acceleration {
    fn default() -> Self {
        Acceleration(Vector2::new(0.0, 0.0))
    }
}

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct Forces(pub Vec<Vector2<f32>>);

#[derive(Component)]
#[storage(VecStorage)]
pub struct CollisionObject {
    pub handle: CollisionObjectSlabHandle,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct CollisionObjectDescription {
    pub behavior: CollisionObjectBehavior,
    pub shape: ShapeHandle<f32>,
}

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct ContactMonitor {
    pub contacts: Vec<Contact>,
}