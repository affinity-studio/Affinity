use std::{io::Cursor, marker::PhantomData};
use serde::{Serialize, de::DeserializeOwned};

pub trait ComponentSerializer<T> {
    fn serialize(data: &mut Vec<u8>, component: &T);
}

pub trait ComponentDeserializer<T> {
    fn deserialize(cursor: &mut Cursor<&[u8]>) -> T;
}

pub struct BincodeComponentSerializer<T> {
    _marker: PhantomData<T>,
}

impl<T> ComponentSerializer<T> for BincodeComponentSerializer<T>
where
    T: Serialize,
{
    fn serialize(data: &mut Vec<u8>, component: &T) {
        bincode::serialize_into(data, component).unwrap();
    }
}

pub struct BincodeComponentDeserializer<T> {
    _marker: PhantomData<T>,
}

impl<T> ComponentDeserializer<T> for BincodeComponentDeserializer<T>
where
    T: DeserializeOwned,
{
    fn deserialize(cursor: &mut Cursor<&[u8]>) -> T {
        bincode::deserialize_from(cursor).unwrap()
    }
}

// Fake deserialization by using Default::default()
pub struct NullComponentDeserializer<T> {
    _marker: PhantomData<T>,
}

impl<T> ComponentDeserializer<T> for NullComponentDeserializer<T>
where
    T: Default,
{
    fn deserialize(_cursor: &mut Cursor<&[u8]>) -> T {
        T::default()
    }
}

/// Serializes no data
pub struct NullComponentSerializer<T> {
    _marker: PhantomData<T>,
}

impl<T> Default for NullComponentSerializer<T> {
    fn default() -> Self {
        NullComponentSerializer {
            _marker: Default::default(),
        }
    }
}

impl<T> ComponentSerializer<T> for NullComponentSerializer<T> {
    fn serialize(_data: &mut Vec<u8>, _component: &T) {}
}