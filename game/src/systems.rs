use nalgebra::Vector2;
use rand::{distributions::Uniform, prelude::*};
use specs::prelude::*;
use std::f32::consts::PI;

use crate::{
    components::{EmitterState, Orientation, Position, Scale},
    core::ecs::SystemName,
    physics::{
        components::{AngularVelocity, Velocity},
    },
    render::components::Texture,
    resources::{DeltaTime, Random},
};

const TEXTURE_ASSETS: [&str; 2] = ["grass", "torch"];

pub struct Emitter;

impl<'a> System<'a> for Emitter {
    #[allow(clippy::type_complexity)]
    type SystemData = (
        WriteStorage<'a, Position>,
        WriteStorage<'a, Orientation>,
        WriteStorage<'a, Scale>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, AngularVelocity>,
        WriteStorage<'a, Texture>,
        WriteStorage<'a, EmitterState>,
        Read<'a, DeltaTime>,
        Write<'a, Random>,
        Entities<'a>,
    );

    fn run(
        &mut self,
        (
            mut positions,
            mut orientations,
            mut scales,
            mut velocities,
            mut angular_velocities,
            mut textures,
            mut emitters,
            dt,
            mut random,
            entities,
        ): Self::SystemData,
    ) {
        struct Spawn {
            position: Position,
            orientation: Orientation,
            angle: f32,
            texture_index: usize,
        }

        let mut spawns = vec![];
        let mut rng = &mut random.rng;
        let angle_distribution = Uniform::new(-PI, PI);
        let texture_distribution = Uniform::new(0, TEXTURE_ASSETS.len());

        for (position, orientation, emitter) in (&positions, &orientations, &mut emitters).join() {
            emitter.time_since_previous += dt.actual;
            while emitter.time_since_previous >= emitter.cooldown {
                spawns.push(Spawn {
                    position: *position,
                    orientation: *orientation,
                    angle: angle_distribution.sample(&mut rng),
                    texture_index: texture_distribution.sample(&mut rng),
                });
                emitter.time_since_previous -= emitter.cooldown;
            }
        }

        for mut spawn in spawns {
            spawn.orientation.0 += spawn.angle;

            entities
                .build_entity()
                .with(spawn.position, &mut positions)
                .with(spawn.orientation, &mut orientations)
                .with(Orientation(0.0), &mut orientations)
                .with(Scale(Vector2::new(1.0, 1.0)), &mut scales)
                .with(
                    Velocity(spawn.orientation.as_direction() * 5.0),
                    &mut velocities,
                )
                .with(AngularVelocity(spawn.angle * 11.0), &mut angular_velocities)
                .with(
                    Texture::named(TEXTURE_ASSETS[spawn.texture_index].to_string()),
                    &mut textures,
                )
                .build();
        }
    }
}

impl SystemName for Emitter {
    const NAME: &'static str = "emitter";
}
