# Affinity

## Requirements

* rustup (<https://rustup.rs/>)
* git-lfs (<https://git-lfs.github.com/>)
* Yarn 1 (classic) (<https://classic.yarnpkg.com/>)
* Powershell

## Build and run

First, `./run.ps1 init` needs to be run to install dependencies. This will also be required with new or updated dependencies.

### Running

```
cargo run --package affinity_dev --release
```

Open <http://localhost:55556/>.

### Running in local-only mode

```
cargo run --package affinity_dev --release -- --disable-server
```

Open <http://localhost:55556/>.

### Additional options

```
cargo run --package affinity_dev --release -- --help
```