use std::sync::atomic::AtomicUsize;

#[derive(Default)]
pub(crate) struct Status {
    pub step: AtomicUsize,
    pub fixed_step: AtomicUsize,
}
