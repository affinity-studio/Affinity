use chrono::Local;
use env_logger::fmt::Formatter;
use log::Record;
use std::{
    io::Write,
    sync::{atomic::Ordering, Arc},
};

use crate::Status;

fn format_log(buf: &mut Formatter, record: &Record, status: &Status) -> std::io::Result<()> {
    use env_logger::fmt::Color::*;
    use log::Level::*;

    let mut style = buf.style();

    write!(buf, "{}", Local::now().format("%H:%M:%S.%f"))?;

    write!(buf, " {}", status.step.load(Ordering::SeqCst))?;

    write!(buf, " {}", status.fixed_step.load(Ordering::SeqCst))?;

    match record.level() {
        Error => style.set_color(Red).set_bold(true),
        Warn => style.set_color(Yellow),
        Info => style.set_color(Green),
        Debug => style.set_color(Blue),
        Trace => style.set_color(Magenta),
    };
    write!(buf, " {}", style.value(record.level()))?;

    write!(buf, " {}", record.module_path().unwrap())?;

    write!(buf, " {}", record.args())?;

    writeln!(buf)
}

pub(crate) fn initialize_logger(status: Arc<Status>) {
    let default_env = env_logger::Env::new().filter_or(
        env_logger::DEFAULT_FILTER_ENV,
        "affinity_server=debug,affinity_game=debug",
    );

    env_logger::Builder::new()
        .format(move |buf, record| format_log(buf, record, &status))
        .write_style(env_logger::WriteStyle::Always)
        .parse_env(default_env)
        .init();
}
