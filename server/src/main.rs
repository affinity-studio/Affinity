use affinity_game::{
    load_level,
    net::{
        msg,
        server::{
            received_state_update::{ReceivedClientStateUpdate, ReceivedClientStateUpdates},
            MessageFromClient, MessageToClient, NetworkEvent, ServerStateUpdates,
        },
        Message,
    },
    resources::{DeltaTime, Random},
    time::DeltaTimeAccumulator,
    GameMode,
};
use log::{debug, info, trace, warn};
use shrev::{EventChannel, ReaderId};
use specs::{Dispatcher, World, WorldExt};
use std::sync::mpsc::Sender;
use std::{
    collections::HashMap,
    sync::{
        atomic::{AtomicBool, AtomicUsize, Ordering},
        mpsc::channel,
        Arc,
    },
    thread,
    time::{Duration, Instant},
};
use std::{fs, sync::mpsc::Receiver};

use crate::{logging::initialize_logger, status::Status};

mod logging;
mod status;

fn main() {
    let status = Arc::new(Status::default());

    initialize_logger(Arc::clone(&status));

    info!("Starting server");

    const ADDRESS: &str = "127.0.0.1:1991";
    let running = Arc::new(AtomicBool::new(true));

    {
        let running = Arc::clone(&running);
        ctrlc::set_handler(move || {
            info!("Received interupt signal. Stopping server...");
            running.store(false, Ordering::SeqCst);
        })
        .expect("Error setting Ctrl-C handler");
    }

    let (game_tx, game_rx) = channel::<GameMessage>();
    let (sender_tx, sender_rx) = channel::<SenderMessage>();

    let listener_thread = {
        info!("Starting listener thread");

        let sender_tx = sender_tx.clone();

        thread::spawn(move || {
            info!("Listening for WebSocket connections on {}", ADDRESS);

            let id_counter = Arc::new(AtomicUsize::new(0));

            ws::listen(ADDRESS, |out| Server {
                out,
                id_counter: Arc::clone(&id_counter),
                client_id: None,
                game_tx: game_tx.clone(),
                sender_tx: sender_tx.clone(),
            })
            .unwrap();
        })
    };

    let sender_thread = {
        info!("Starting sender thread");

        thread::spawn(move || {
            let mut clients = HashMap::new();

            for msg in sender_rx {
                match msg {
                    SenderMessage::ClientConnected(id, out) => {
                        debug!("Updating client map with client {}", id);
                        clients.insert(id, out);
                    }
                    SenderMessage::MessageToClient(MessageToClient { client_id, message }) => {
                        if let Some(out) = &clients.get(&client_id) {
                            let serialized = serde_json::to_string(&message).unwrap();

                            debug!(
                                "Sending text message to client {}: {}",
                                client_id, serialized
                            );

                            out.send(ws::Message::Text(serialized)).unwrap();
                        }
                    }
                    SenderMessage::BinaryUpdate(client_id, update) => {
                        trace!(
                            "Sending binary message to client {} of length {}",
                            client_id,
                            update.len()
                        );

                        if let Some(out) = clients.get(&client_id) {
                            out.send(ws::Message::Binary(update)).unwrap();
                        } else {
                            debug!("Tried sending binary message to client {}, but client is not mapped", client_id);
                        }
                    }
                    SenderMessage::ClientDisconnected(id) => {
                        debug!("Removing client {} from mapping", id);
                        clients.remove(&id);
                    }
                }
            }
        })
    };

    let game_thread = {
        info!("Starting game thread");

        let running = Arc::clone(&running);
        let status = Arc::clone(&status);

        thread::spawn(move || {
            info!("Starting game loop");

            let mut game = Game::new(game_rx, sender_tx, status);
            game.load_level();

            let mut previous = Instant::now();
            while running.load(Ordering::SeqCst) {
                let now = Instant::now();
                let duration = now.duration_since(previous);
                previous = now;

                game.step(duration);
            }
        })
    };

    game_thread.join().unwrap();
    sender_thread.join().unwrap();
    listener_thread.join().unwrap();
}

#[derive(Debug)]
enum GameMessage {
    MessageFromClient(MessageFromClient),
    ClientConnected(u32),
    ClientDisconnected(u32),
    ClientStateUpdate(u32, Vec<u8>),
}

#[derive(Debug)]
enum SenderMessage {
    MessageToClient(MessageToClient),
    ClientConnected(u32, ws::Sender),
    ClientDisconnected(u32),
    BinaryUpdate(u32, Vec<u8>),
}

struct Server {
    out: ws::Sender,
    id_counter: Arc<AtomicUsize>,
    client_id: Option<u32>,
    game_tx: Sender<GameMessage>,
    sender_tx: Sender<SenderMessage>,
}

impl Server {
    fn get_client_id(&self) -> ws::Result<u32> {
        self.client_id
            .ok_or_else(|| ws::Error::new(ws::ErrorKind::Internal, "client_id is not set"))
    }
}

impl ws::Handler for Server {
    fn on_request(&mut self, req: &ws::Request) -> ws::Result<ws::Response> {
        let mut res = ws::Response::from_request(req)?;
        if req.protocols()?.contains(&"awsp") {
            debug!("New connection with awsp protocol");
            res.set_protocol("awsp");
        }

        Ok(res)
    }

    fn on_open(&mut self, handshake: ws::Handshake) -> ws::Result<()> {
        let address = if let Some(address) = handshake.remote_addr()? {
            address
        } else {
            "unknown".to_string()
        };

        let id = self.id_counter.fetch_add(1, Ordering::SeqCst) as u32;
        self.client_id = Some(id);

        debug!("Connection from {} opened and assigned ID {}", address, id);

        self.sender_tx
            .send(SenderMessage::ClientConnected(id, self.out.clone()))
            .unwrap();

        self.game_tx.send(GameMessage::ClientConnected(id)).unwrap();

        self.sender_tx
            .send(SenderMessage::MessageToClient(MessageToClient::new(
                id,
                Message::Connected(msg::Connected { cid: id }),
            )))
            .unwrap();

        Ok(())
    }

    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        trace!("Handling message");

        let client_id = self.get_client_id()?;

        match msg {
            ws::Message::Text(text) => {
                debug!("Received text from client {}: {}", client_id, text);

                if let Ok(msg) = serde_json::from_str::<Message>(&text) {
                    let msg_from_client = MessageFromClient::new(client_id, msg);
                    self.game_tx
                        .send(GameMessage::MessageFromClient(msg_from_client))
                        .unwrap();
                } else {
                    warn!("Message is unknown. Ignoring");
                }
            }
            ws::Message::Binary(bin) => {
                trace!("Received binary message of length {}", bin.len());

                if bin.is_empty() {
                    warn!("Client sent empty binary message. Ignoring.");
                }

                let message_type = bin[0];
                if message_type == 1 {
                    self.game_tx
                        .send(GameMessage::ClientStateUpdate(client_id, bin[1..].to_vec()))
                        .unwrap();
                }
            }
        }

        Ok(())
    }

    fn on_close(&mut self, code: ws::CloseCode, reason: &str) {
        use ws::CloseCode;

        let reason = match code {
            CloseCode::Normal => "The client is done with the connection".to_string(),
            CloseCode::Away => "The client is leaving the site".to_string(),
            _ => format!("The client encountered an error: {}", reason),
        };

        let id = self.get_client_id().unwrap();
        info!("Client {} disconnected: {}", id, reason);

        self.sender_tx
            .send(SenderMessage::ClientDisconnected(id))
            .unwrap();
        self.game_tx
            .send(GameMessage::ClientDisconnected(id))
            .unwrap();
    }
}

struct Game<'a, 'b> {
    world: World,
    dispatcher: Dispatcher<'a, 'b>,
    accumulator: DeltaTimeAccumulator,
    msg_to_client_reader_id: ReaderId<MessageToClient>,
    game_rx: Receiver<GameMessage>,
    sender_tx: Sender<SenderMessage>,
    status: Arc<Status>,
}

impl<'a, 'b> Game<'a, 'b> {
    fn new(
        game_rx: Receiver<GameMessage>,
        sender_tx: Sender<SenderMessage>,
        status: Arc<Status>,
    ) -> Game<'a, 'b> {
        let world = affinity_game::create_world(Random::default(), GameMode::Server);

        let dispatcher_builder = affinity_game::create_dispatcher_builder(&world, GameMode::Server);
        let dispatcher = dispatcher_builder.build();

        let msg_to_client_reader_id = world
            .write_resource::<EventChannel<MessageToClient>>()
            .register_reader();

        Game {
            world,
            dispatcher,
            accumulator: DeltaTimeAccumulator::new(Duration::from_nanos(16666666)),
            msg_to_client_reader_id,
            game_rx,
            sender_tx,
            status,
        }
    }

    fn load_level(&mut self) {
        let level_path = "./assets/test_level.json";
        let level_bytes = fs::read(&level_path).expect("Could not read level file");
        load_level(&mut self.world, &level_bytes, GameMode::Server).expect("Could not load level");
    }

    fn step(&mut self, mut deltatime: Duration) {
        trace!("Step start");
        const DT_MAX: Duration = Duration::from_millis(250);
        if deltatime > DT_MAX {
            deltatime = DT_MAX;
        }

        self.step_fixed(deltatime);
        // self.step_loose(deltatime);

        trace!("Step end");

        self.status.step.fetch_add(1, Ordering::SeqCst);

        let remaining = self.accumulator.remaining();
        if remaining.as_micros() > 1 {
            trace!("Sleeping for {} us", remaining.as_micros());
            std::thread::sleep(remaining);
        }
    }

    fn step_fixed(&mut self, dt: Duration) {
        self.accumulator.accumulate(dt);
        if self.accumulator.is_consumable() {
            {
                let mut deltatime = self.world.write_resource::<DeltaTime>();
                deltatime.actual = dt;
                deltatime.fixed = Some(self.accumulator.consumption_step());
            }

            while self.accumulator.consume() {
                trace!("Fixed step start");
                self.dispatcher.dispatch(&self.world);
                self.world.maintain();
                trace!("Fixed step end");
                self.status.fixed_step.fetch_add(1, Ordering::SeqCst);
            }

            self.handle_inbound_messages();
            self.handle_outbound_messages();
            self.send_state_updates();
        }
    }

    fn step_loose(&mut self, dt: Duration) {
        {
            let mut deltatime = self.world.write_resource::<DeltaTime>();
            deltatime.actual = dt;
            deltatime.fixed = None;
        }

        // TODO: Separate dispatcher for loose and fixed updates
        self.dispatcher.dispatch(&self.world);
        self.world.maintain();
    }

    fn handle_new_client(&self, client_id: u32) {
        let mut net_events = self.world.write_resource::<EventChannel<NetworkEvent>>();
        net_events.single_write(NetworkEvent::ClientConnected(client_id));
    }

    fn handle_leaving_client(&self, client_id: u32) {
        let mut net_events = self.world.write_resource::<EventChannel<NetworkEvent>>();
        net_events.single_write(NetworkEvent::ClientDisconnected(client_id));
    }

    fn handle_incoming_message(&self, message: MessageFromClient) {
        let mut net_events = self.world.write_resource::<EventChannel<NetworkEvent>>();
        net_events.single_write(NetworkEvent::MessageFromClient(message));
    }

    fn handle_client_state_update(&self, client_id: u32, update: Vec<u8>) {
        let mut received_client_updates = self.world.write_resource::<ReceivedClientStateUpdates>();
        received_client_updates
            .client_updates
            .insert(client_id, ReceivedClientStateUpdate::new(update));
    }

    fn handle_inbound_messages(&self) {
        for msg in self.game_rx.try_iter() {
            match msg {
                GameMessage::ClientConnected(client_id) => {
                    self.handle_new_client(client_id);
                }
                GameMessage::MessageFromClient(msg) => {
                    self.handle_incoming_message(msg);
                }
                GameMessage::ClientStateUpdate(client_id, update) => {
                    self.handle_client_state_update(client_id, update);
                }
                GameMessage::ClientDisconnected(client_id) => {
                    self.handle_leaving_client(client_id);
                }
            }
        }
    }

    fn handle_outbound_messages(&mut self) {
        let msg_event_chan = self.world.read_resource::<EventChannel<MessageToClient>>();
        for msg in msg_event_chan.read(&mut self.msg_to_client_reader_id) {
            self.sender_tx
                .send(SenderMessage::MessageToClient(msg.clone()))
                .unwrap();
        }
    }

    fn send_state_updates(&self) {
        let mut game_state_updates = self.world.write_resource::<ServerStateUpdates>();
        for (client_id, state_update) in &mut game_state_updates.client_updates {
            if let Some(state_update) = state_update.finalize() {
                let mut update = vec![0];
                update.reserve(state_update.len());
                update.extend_from_slice(state_update);
                self.sender_tx
                    .send(SenderMessage::BinaryUpdate(*client_id, update))
                    .unwrap();
            }
        }
    }
}
