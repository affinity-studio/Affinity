use affinity_game::{
    core::ecs::SystemName,
    resources::{DeltaTime, Random},
    physics::systems::VelocityApplier, GameMode,
};
use specs::{Dispatcher, DispatcherBuilder, World, WorldExt};
use std::time::{Duration, Instant};

fn main() {
    let mut game = Game::new();

    let mut previous = Instant::now();
    loop {
        let now = Instant::now();
        let duration = now.duration_since(previous);
        previous = now;

        game.step(duration);
    }
}

struct Game<'a, 'b> {
    world: World,
    dispatcher: Dispatcher<'a, 'b>,
}

impl<'a, 'b> Game<'a, 'b> {
    fn new() -> Game<'a, 'b> {
        let world = affinity_game::create_world(Random::default(), GameMode::Local);

        let dispatcher = DispatcherBuilder::new()
            .with(VelocityApplier, VelocityApplier::NAME, &[])
            .build();

        Game { world, dispatcher }
    }

    fn step(&mut self, deltatime: Duration) {
        {
            let mut deltatime_res = self.world.write_resource::<DeltaTime>();
            deltatime_res.actual = deltatime;
        }

        self.dispatcher.dispatch(&self.world);
    }
}
